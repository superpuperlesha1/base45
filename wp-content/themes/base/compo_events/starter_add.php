<!-- Modal CREATE EVENT -->
<div class="modal fade" id="ds_float_cevent_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Create a Get Together', 'base') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="ds_float_cevent_modal_box"></div>
                <div id="event_crt_psubmmessg" class="text-danger"></div>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                <button type="button" class="btn btn-primary" id="uevent_crt_submitteer"><?= __('Create Event', 'base') ?></button>
            </div>
        </div>
    </div>
</div>

<!-- Modal EVENT EDIT -->
<div class="modal fade " id="ds_float_eventedit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel7" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Edit event', 'base') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="ds_float_eventedit_modal_box"><?= __('Content edit event...', 'base') ?></div>
                <div id="event_upd_psubmmessg" class="text-danger"></div>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                <button type="button" class="btn btn-primary" id="uevent_edit_submitteer"><?= __('Save Event', 'base') ?></button>
            </div>
        </div>
    </div>
</div>

<!-- CONFIRMATION POPUP-->
<div class="modal fade" id="ds_confirm_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Confirmation Popup', 'base') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                <button type="button" id="dc_singl_event_delete_submitter" data-eventid="" data-newURL="<?= esc_url(get_the_permalink(get_field('sopt_events_pid', 'option'))) ?>" class="btn btn-primary"><?= __('Continue', 'base') ?></button>
            </div>
        </div>
    </div>
</div>


<script>
	//jQuery(document).ready(function($){
	document.addEventListener('DOMContentLoaded', function(){

		//===REGISTER EVENT autocomplitter init===
		function init121(){
			if(document.getElementById('event_crt_place') != null ){
				var input = document.getElementById('event_crt_place');
				var autocomplete = new google.maps.places.Autocomplete(input, AutocompleteOpt);
				google.maps.event.addListener(autocomplete, 'place_changed', function () {
					var place = autocomplete.getPlace();
					document.getElementById('event_crt_place_title').value = '';
					document.getElementById('event_crt_place_lt').value    = '';
					document.getElementById('event_crt_place_ln').value    = '';
					place.address_components.forEach(function(item, i, arr) {
						if( item.types.indexOf('postal_code') > -1 ){
							document.getElementById('event_crt_place_title').value = place.name;
							document.getElementById('event_crt_place_lt').value    = place.geometry.location.lat();
							document.getElementById('event_crt_place_ln').value    = place.geometry.location.lng();
						}
					});
				});
			}
		}
		init121();

		//===single EVENT MAP===
		function initialize_view_event() {
			if( typeof singl_event_map_lt !== 'undefined' && typeof singl_event_map_ln !== 'undefined' ){
				//alert(singl_event_map_lt);
				const myLatLng = { lat: singl_event_map_lt, lng: singl_event_map_ln };
				const map = new google.maps.Map(document.getElementById('singl_event_map'), {
					zoom: 10,
					center: myLatLng,
			    });
			    new google.maps.Marker({
					position: myLatLng,
					map,
					title:    singl_event_title,
			    });
			}
		}
		initialize_view_event();

		//===DATA-TIME-PICKER STARTER
		function initDTPicker(){
			$('#event_crt_date').datepicker({minDate:new Date(), timepicker:true, altFieldDateFormat:'@', altField:'#event_crt_date_ts', language:'en',});
		}
		initDTPicker();

		//===CREATE EVENT===
		$(document).on('click', '.dc_event_create', function(){
			$('#ds_float_cevent_modal').modal();
			$('#ds_float_cevent_modal_box').html(Loading);
			$.ajax({
					type: 'POST',
					url:  WPajaxURL,
					data: {
							action: 'loadcleareventform',
						  },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#ds_float_cevent_modal_box').html(data);
						initDTPicker();
						init121();
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
		});

		//===MENU EVENTS===
		function w51p_menu_events(pg='', past='', pURL){
			setURLpage(pURL);
			$('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
			$.ajax({
				type:   'POST',
				url:    WPajaxURL,
				data:{
						action: 'menuevents',
						pg:     pg,
						past:   past
					 },
				success:     function(data, textStatus, XMLHttpRequest) {
					$('#base_main').html(data);
					$('#base_main').fadeTo('slow', 1, function(){  });
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		}

		$(document).on('click', '.w51p_menuin_events', function(){
			event.preventDefault();
			var pURL = $(this).attr('href');
			w51p_menu_events(1, 0, pURL);
		});

		//===MENU page EVENTS===
		$(document).on('click', '.w51p_menu_events', function(){
			event.preventDefault();
			var pURL = $(this).find('a').attr('href');
			w51p_menu_events(1, 0, pURL);
		});

		//===PAGING page EVENTS past===
		$(document).on('click', '.paging_events_p', function(){
			event.preventDefault();
			var pURL = $(this).find('a').attr('href');
			var pg = $(this).attr('data-pg');
			w51p_menu_events(pg, 1, pURL);
		});

		//===PAGING page EVENTS featur===
		$(document).on('click', '.paging_events', function(){
			event.preventDefault();
			var pg = $(this).attr('data-pg');
			w51p_menu_events(pg, 0);
		});

		$(document).on('click', '.w51p_url_event', function(event){
			event.preventDefault();
			setURLpage($(this).attr('href'));
			var eventid = $(this).attr('data-eventid');
			openEvent(eventid);
		});

		//===open EVENT===
		function openEvent(eventid=0, title='', content='', scrolltopmsg=false){
			$('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
			$.ajax({
				type:   'POST',
				url:    WPajaxURL,
				data:{
						action:  'openevent',
						eventid: eventid,
					 },
				success: function(data, textStatus, XMLHttpRequest) {
					$('#base_main').html(data);
					$('#base_main').fadeTo('slow', 1, function(){  });
					initialize_view_event();
					$('#gmsg_title').val(title);
					$('#gmsg_txt').val(content);
					var sc = $('#PMSGtopper');
					if(sc.length && scrolltopmsg){
						sc = $(sc).offset().top-50;
						$('html, body').animate({scrollTop: sc}, 1000);
					}
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					top.location.href = WPURL;
				}
			});
		}

		//===CONFIRMATION POPUP===
		$(document).on('click', '.dc_confirm_open', function(){
			$('#ds_confirm_modal').modal();
			$('#dc_singl_event_delete_submitter').attr('data-eventid', $(this).attr('data-eventid'));
		});

		$(document).on('click', '.sevnt_user_yn_submitter', function(){
			var eventid    = $(this).attr('data-eventid');
			var evenaccept = $(this).attr('data-commit');
			$('#base_main').html(Loading);
			$.ajax({
					type:   'POST',
					url:    WPajaxURL,
					data:{
							action:     'yngotoevent',
							eventid:    eventid,
							evenaccept: evenaccept,
						 },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#base_main').html(data);
						initialize_view_event();
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
		});

		//===DELETE EVENT===
		$(document).on('click', '#dc_singl_event_delete_submitter', function(){
			var eventid = $(this).attr('data-eventid');
			var newURL  = $(this).attr('data-newURL');
			$('#ds_confirm_modal').modal('hide');
			$('#base_main').html(Loading);
			$.ajax({
					type:   'POST',
					url:    WPajaxURL,
					data:{
							action:  'deleteevent',
							eventid: eventid,
						 },
					success: function(data, textStatus, XMLHttpRequest) {
						$('#base_main').html(data);
						setURLpage(newURL);
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
		});

		//===UPDATE EVENT===
		$(document).on('click', '#uevent_edit_submitteer', function(){
			var err = '';

			var sevnt_thumb = document.getElementById('sevnt_thumb');
			if(sevnt_thumb.files.length){
				var fileName = sevnt_thumb.files[0].name;
				var fileSize = parseInt(sevnt_thumb.files[0].size);
			}else{
				var fileName = '';
				var fileSize = 0;
			}
			var arr         = ['', '.jpg', '.jpeg', '.png'];
			var pposs       = fileName.indexOf('.');
			var fext        = fileName.substring(pposs);

			$('#event_crt_place').removeClass('border-danger');
			$('#event_crt_name').removeClass('border-danger');
			$('#event_crt_content').removeClass('border-danger');
			$('#event_crt_date_ts').removeClass('border-danger');

			var event_crt_name        = $('#event_crt_name').val();
			var event_crt_content     = $('#event_crt_content').val();
			var event_crt_date_ts     = $('#event_crt_date_ts').val();
			var event_crt_place_title = $('#event_crt_place_title').val();
			var event_crt_place_ln    = $('#event_crt_place_ln').val();
			var event_crt_place_lt    = $('#event_crt_place_lt').val();
			var uevent_crt_eventid    = $('#uevent_crt_eventid').val();

			if( arrIMGt.indexOf(fext) == -1 ){
				err=$('#sevnt_thumb').attr('data-ftype');
			}
			if( fileSize > 2000000 ){
				err=$('#sevnt_thumb').attr('data-fsize');
			}
			if(event_crt_name.length        < 6 ){
				err='Please enter your "Get Together name".';
				$('#event_crt_name').addClass('border-danger');
			}
			if(event_crt_content.length     < 60){
				err='Please enter your "Message to the invitees".';
				$('#event_crt_content').addClass('border-danger');
			}
			if(event_crt_date_ts.length     < 10){
				err='Please enter event "Date Time".';
				$('#event_crt_date_ts').addClass('border-danger');
			}
			//if(event_crt_place_title.length < 3){
				//err='Enter a more detailed address.';
				//$('#event_crt_place').addClass('border-danger');
			//}
			//if(event_crt_place_ln.length    < 3){
				//err='Enter a more detailed address.';
				//$('#event_crt_place').addClass('border-danger');
			//}
			//if(event_crt_place_lt.length    < 3){
				//err='Enter a more detailed address.';
				//$('#event_crt_place').addClass('border-danger');
			//}

			if(err==''){
				$('#event_upd_psubmmessg').html(Loading);
				var form_data = new FormData(document.getElementById('event_edit_form'));
				form_data.append('action', 'updateevent');
				$.ajax({
						type:        'POST',
						url:         WPajaxURL,
						data:        form_data,
						contentType: false,
		        		processData: false,
						success:     function(data, textStatus, XMLHttpRequest) {
							data = JSON.parse(data);
							if( data.txt.length ){
								$('#event_upd_psubmmessg').html(data.txt);
							}else{
								$('#ds_float_eventedit_modal').modal('hide');
								$('#ds_float_cevent_modal').modal('hide');
								openEvent(data.id);
							}
						},
						error: function(MLHttpRequest, textStatus, errorThrown) {
							top.location.href = WPURL;
						}
					});
			}else{
				$('#event_upd_psubmmessg').html(err);
			}
		});

		//===START EDIT EVENT===
		$(document).on('click', '.dc_event_edit', function(){
			$('#ds_float_eventedit_modal').modal();
			$('#ds_float_eventedit_modal_box').html(Loading);
			var eventid = $(this).attr('data-eventid');
			$.ajax({
					type:        'POST',
					url:         WPajaxURL,
					data:{
								action:  'getformeditevent',
								eventid: eventid,
						 },
					success:     function(data, textStatus, XMLHttpRequest) {
						$('#ds_float_eventedit_modal_box').html(data);
						$('#event_upd_psubmmessg').html('');
						initDTPicker();
						init121();
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
		});

		//===CREATE EVENT===
		$(document).on('click', '#uevent_crt_submitteer', function(){
			var err = '';

			var sevnt_thumb = document.getElementById('sevnt_thumb');
			if(sevnt_thumb.files.length){
				var fileName = sevnt_thumb.files[0].name;
				var fileSize = parseInt(sevnt_thumb.files[0].size);
			}else{
				var fileName = '';
				var fileSize = 0;
			}
			var arr         = ['', '.jpg', '.jpeg', '.png'];
			var pposs       = fileName.indexOf('.');
			var fext        = fileName.substring(pposs);

			$('#event_crt_place').removeClass('border-danger');
			$('#event_crt_name').removeClass('border-danger');
			$('#event_crt_content').removeClass('border-danger');
			$('#event_crt_date_ts').removeClass('border-danger');

			var event_crt_name        = $('#event_crt_name').val();
			var event_crt_content     = $('#event_crt_content').val();
			var event_crt_date_ts     = $('#event_crt_date_ts').val();
			var event_crt_place       = $('#event_crt_place').val();
			var event_crt_place_title = $('#event_crt_place_title').val();
			var event_crt_publiczone  = $('#event_crt_publiczone').val();
			var event_crt_euser_1     = $('#event_crt_euser_1').val();
			var event_crt_euser_2     = $('#event_crt_euser_2').val();
			var event_crt_euser_3     = $('#event_crt_euser_3').val();
			var event_crt_place_ln    = $('#event_crt_place_ln').val();
			var event_crt_place_lt    = $('#event_crt_place_lt').val();
			var uevent_crt_eventid    = $('#uevent_crt_eventid').val();


			if( arrIMGt.indexOf(fext) == -1 ){
				err=$('#sevnt_thumb').attr('data-ftype');
			}
			if( fileSize > 2000000 ){
				err=$('#sevnt_thumb').attr('data-fsize');
			}
			if(event_crt_name.length        <  6 ){
				err='Please enter "Get Together name".';
				$('#event_crt_name').addClass('border-danger');
			}
			if(event_crt_content.length     <  60){
				err='Please enter "Message to the invitees".';
				$('#event_crt_content').addClass('border-danger');
			}
			if(event_crt_date_ts.length     <  10){
				err='Please enter event "Date Time".';
				$('#event_crt_date_ts').addClass('border-danger');
			}
			//if(event_crt_place_title.length < 3){
				//err='Enter a more detailed address.';
				//$('#event_crt_place').addClass('border-danger');
			//}
			//if(event_crt_place_ln.length    < 3){
				//err='Enter a more detailed address.';
				//$('#event_crt_place').addClass('border-danger');
			//}
			//if(event_crt_place_lt.length    < 3){
				//err='Enter a more detailed address.';
				//$('#event_crt_place').addClass('border-danger');
			//}

			var form_data = new FormData(document.getElementById('event_crt_form'));
			form_data.append('action', 'createevent');
			if(err==''){
				$('#event_crt_psubmmessg').html(Loading);
				$.ajax({
					type:        'POST',
					url:         WPajaxURL,
					data:        form_data,
					contentType: false,
	        		processData: false,
					success:     function(data, textStatus, XMLHttpRequest) {
						data = JSON.parse(data);
						if( data.txt.length ){
							$('#event_crt_psubmmessg').html(data.txt);
						}else{
							$('#ds_float_eventedit_modal').modal('hide');
							$('#ds_float_cevent_modal').modal('hide');
							openEvent(data.id);
						}
					},
					error: function(MLHttpRequest, textStatus, errorThrown) {
						top.location.href = WPURL;
					}
				});
			}else{
				$('#event_crt_psubmmessg').html(err);
			}
		});
	});
</script>