<?php

/*===MENU Page [GMSG]===
$id = get_field('sopt_gmsgs_pid', 'option')
<li class="w51p_menu_gmsgs">
	<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
</li>*/

add_action( 'wp_footer', 'action_function_name_2477' );
function action_function_name_2477(){
	get_template_part('/compo_gmessages/starter_add');
}


//===PAGE GROUP GCHAT===
function page_gchat($profileuser=0, $ErrMSG=''){
    //===CHECK PAYMENT===
    $checkUserCS = checkUserCS(true);
    if($checkUserCS){
        echo $checkUserCS;
        exit();
    }

	//===VALIDATION===
	$profileuser = (int)$profileuser;
	if($profileuser<1){
		$profileuser = 0;
	}

	//===set I READ all messages to this EVENT===
	$arr = (array)unserialize( get_user_meta(get_current_user_id(), 'new_gmsg', true) );
	unset($arr[$profileuser]);
	update_user_meta( get_current_user_id(), 'new_gmsg', serialize($arr) );

	$IDListing2  = 'e'.$profileuser.'e'; ?>

    <div id="PMSGtopper" class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?= __('Discussion', 'base') ?></h2>
            </div>
            <div class="col-md-12">
                <form action="#uID" method="POST" class="form form-event-single">
                     <div class="form-group">
                        <div      id="gmsg_notation_box"       class="text-danger"><?= $ErrMSG ?></div>
                    </div>
                    <div class="form-group">
                        <span     id="gmsg_reply_parent_title" class="text-success"></span>
                    </div>
                    <div class="form-group">
                        <span     id="gmsg_reply_cancel"       class="fa fa-times text-success d-none" title="<?= __('Cancel reply message!', 'base') ?>"></span>
                        <input    id="gmsg_reply_parent_id"    name="gmsg_reply_parent_id" class="form-control" type="hidden">
                        <input    id="gmsg_title"              name="gmsg_title"       type="text"  maxlength="1000" class="form-control" placeholder="<?= __('Subject', 'base') ?>">
                    </div>
                    <div class="form-group">
                        <textarea id="gmsg_txt"                name="gmsg_txt"  maxlength="5000" class="form-control" data-errmsg="<?= __('Please enter your Message.', 'base') ?>" placeholder="<?= __('Message', 'base') ?>"></textarea>
                    </div>
                    <div class="form-group">
                        <input    id="gmsg_profileuser"        name="gmsg_profileuser" type="hidden" value="<?= $profileuser ?>">
                        <input    id="gmsg_IDListing2"         name="gmsg_IDListing2"  type="hidden" value="<?= $IDListing2 ?>">
                        <button   id="gmsg_submit_post" class="btn btn-primary" type="submit" data-errmsg="<?= __('Please enter your Message.', 'base') ?>"><?= __('Send', 'base') ?></button>
                    </div>
                </form>

                <form id="getCountPMSG_form" action="#uID" method="POST">
                    <input type="hidden" name="getCountGMSG" id="getCountGMSG" value="<?= getCountGMSG($profileuser) ?>">
                    <input type="hidden" name="eventID"      id="eventID"      value="<?= $profileuser ?>">
                </form>
            </div>
            <div class="col-md-12"><?php
    			//===getting all messages===
    			$postsall = get_posts([ 'numberposts'      => 51,
                                        'orderby'          => 'date',
                                        'order'            => 'DESC',
                                        'post_type'        => 'gmsg',
                                        'fields'           => '',
                                        'meta_query' => [
                                            [
                                                [
                                                    'key'     => 'fromto',
                                                    'value'   => $IDListing2,
                                                    'compare' => '=',
                                                ]
                                            ]
                                        ]
                                    ]);
    			foreach( $postsall as $postpar ){
    				if($postpar->post_parent==0){
    					$userpostdata = get_user_by('id', $postpar->post_author); ?>
    					<div class="row">
    			            <div class="col-md-3">
                                <a href="<?= get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $postpar->post_author ?>" class="event-single__msg-img left dc_myacc_view" data-userid="<?= $postpar->post_author ?>">
                                    <img  src="<?= get_myavatar_url($postpar->post_author, 'thumbnail_64x64') ?>" alt="Ava" width="64" height="64">
                                </a>
    			            </div>
                            <div class="col-md-9">
                                <h4><?= get_the_title($postpar->ID) ?></h4>
    			            	<h5><?= $userpostdata->first_name ?></h5>
    							<div><?= apply_filters('the_content', $postpar->post_content) ?></div>
    							<time><?= get_the_time(get_option('date_format')) ?></time><?php

    							if($postpar->post_author == get_current_user_id()){ ?>
    								<i class="text-danger gmsg_deleter" data-msgid="<?= $postpar->ID ?>" data-profileuser="<?= $profileuser ?>" aria-hidden="true" title="<?= __('Delete message!', 'base') ?>" data-toggle="tooltip" data-placement="top">X</i><?php
    							}

    							if($postpar->post_author != get_current_user_id()){ ?>
    								<img src="<?= esc_url(get_template_directory_uri()) ?>//img/icon-arrow-left.svg" class="gmsg_reply" data-parmesid="<?= $postpar->ID ?>" title="<?= __('Reply message!', 'base') ?>" alt="reply"><?php
    							} ?>
    			            </div>
    			        </div><?php

    				    foreach( $postsall as $postdoch ){
    						if($postdoch->post_parent==$postpar->ID){
    							$userpostdata = get_user_by('id', $postdoch->post_author); ?>
    							<div class="row">
    					            <div class="col-ms-3">
    					            	<a href="<?= get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) ?>/?profile=<?= $postdoch->post_author ?>" class="event-single__msg-img right dc_myacc_view" data-userid="<?= $postdoch->post_author ?>">
                                            <img  src="<?= get_myavatar_url($postdoch->post_author, 'thumbnail_64x64') ?>" alt="Ava" width="64" height="64">
                                        </a>
    					            </div>
                                    <div class="col-ms-9">
        					            <h4><?= get_the_title($postdoch->ID) ?></h4>
                                        <h5><?= $userpostdata->first_name ?></h5>
                                        <p><?= apply_filters('the_content', $postdoch->post_content) ?></p>
                                        <time><?= get_the_time(get_option('date_format')) ?></time><?php
        				        		if($postdoch->post_author == get_current_user_id()){ ?>
        									<i class="text-danger gmsg_deleter" data-msgid="<?= $postdoch->ID ?>" data-profileuser="<?= $profileuser ?>" aria-hidden="true" title="<?= __('Delete message!', 'base') ?>" data-toggle="tooltip" data-placement="top">X</i><?php
        								} ?>
                                    </div>
    					        </div><?php
    						}
    					}
    				}
    			} ?>
            </div>
        </div>
    </div><?php
}


function getCountGMSG($eventID=0){
    $eventID = (int)$eventID;
    $query = new WP_Query([ 'numberposts'      => -1,
                            'orderby'          => 'date',
                            'order'            => 'DESC',
                            'post_type'        => 'gmsg',
                            'fields'           => 'ids',
                            'meta_query' => [
                                [
                                    [
                                        'key'     => 'fromto',
                                        'value'   => 'e'.$eventID.'e',
                                        'compare' => '=',
                                    ]
                                ]
                            ]
                        ]);
    return $query->found_posts;
}


//===write gmsg===
function write_gmsg($profileuser, $pmsg_title = '', $pmsg_txt = '', $pmsg_reply_parent_id = 0, $IDListing2 = ''){
    $res = '';

    $gmsgCommit = true;
    if (get_current_user_id() == (int)get_post_meta($profileuser, 'sevnt_user_1', true) && (int)get_post_meta($profileuser, 'sevnt_user_1_y', true) != 1) {
        $gmsgCommit = false;
    }
    if (get_current_user_id() == (int)get_post_meta($profileuser, 'sevnt_user_2', true) && (int)get_post_meta($profileuser, 'sevnt_user_2_y', true) != 1) {
        $gmsgCommit = false;
    }
    if (get_current_user_id() == (int)get_post_meta($profileuser, 'sevnt_user_3', true) && (int)get_post_meta($profileuser, 'sevnt_user_3_y', true) != 1) {
        $gmsgCommit = false;
    }

    //===insert message===
    if (strlen($pmsg_txt) > 36) {
        $post_id = wp_insert_post([
            'post_title'   => substr(sanitize_text_field($pmsg_title), 0, 1000),
            'post_content' => substr(sanitize_text_field($pmsg_txt), 0, 10000),
            'post_status'  => 'publish',
            'post_author'  => get_current_user_id(),
            'post_type'    => 'gmsg',
            'post_parent'  => (int)$pmsg_reply_parent_id,
        ]);
        $res = __('Message added!', 'base');

        //===LOG BLOCK usr_cf_1===
        //if group message usr_cf_2
        update_post_meta($post_id, 'fromto', (string)$IDListing2);

        $user1 = get_post_meta($profileuser, 'sevnt_user_1', true);
        $user2 = get_post_meta($profileuser, 'sevnt_user_2', true);
        $user3 = get_post_meta($profileuser, 'sevnt_user_3', true);

        $user1 = get_user_by('id', $user1);
        $user2 = get_user_by('id', $user2);
        $user3 = get_user_by('id', $user3);

        if ($user1 && $user1->ID != get_current_user_id()) {
            $arr = (array)unserialize(get_user_meta($user1->ID, 'new_gmsg', true));
            $arr[$profileuser] = 1;
            update_user_meta($user1->ID, 'new_gmsg', serialize($arr));
            if ((int)get_user_meta($user1->ID, 'usr_cf_2', true)) {
                setLogEmail($user1->ID, __('You received a group message.', 'base'));
            }
            setLog($user1->ID, __('You received a group message.', 'base'));
        }
        if ($user2 && $user2->ID != get_current_user_id()) {
            $arr = (array)unserialize(get_user_meta($user2->ID, 'new_gmsg', true));
            $arr[$profileuser] = 1;
            update_user_meta($user2->ID, 'new_gmsg', serialize($arr));
            if ((int)get_user_meta($user2->ID, 'usr_cf_2', true)) {
                setLogEmail($user2->ID, __('You received a group message.', 'base'));
            }
            setLog($user2->ID, __('You received a group message.', 'base'));
        }
        if ($user3 && $user3->ID != get_current_user_id()) {
            $arr = (array)unserialize(get_user_meta($user3->ID, 'new_gmsg', true));
            $arr[$profileuser] = 1;
            update_user_meta($user3->ID, 'new_gmsg', serialize($arr));
            if ((int)get_user_meta($user3->ID, 'usr_cf_2', true)) {
                setLogEmail($user3->ID, __('You received a group message.', 'base'));
            }
            setLog($user3->ID, __('You received a group message.', 'base'));
        }
        //=========================
    } else {
        $res = __('Please enter "Message".', 'base');
    }

    return $res;
}


//===DELETE group mssage===
add_action('wp_ajax_getgmsg',        'getgmsg');
//add_action('wp_ajax_nopriv_getgmsg', 'getgmsg');
function getgmsg(){
    $_POST['pageid'] = $_POST['pageid'] ?? '';
    
    page_gchat( (isset($_POST['pageid']) ?$_POST['pageid'] :0) );
    
    die();
}


//===DELETE group mssage===
add_action('wp_ajax_delgmsg',        'delgmsg');
//add_action('wp_ajax_nopriv_delgmsg', 'delgmsg');
function delgmsg(){
    $ErrMSG='';
    $_POST['profileuser'] = $_POST['profileuser'] ?? '';
    $_POST['msgid']       = $_POST['msgid']       ?? '';

    $post = get_post((int)$_POST['msgid']);
    if($post){
        if($post->post_author == get_current_user_id()){
            wp_delete_post($post->ID);
            $ErrMSG = __('Group message deleted!', 'base');
        }else{
            $ErrMSG = __('You are no author this group message!', 'base');
        }
    }else{
        $ErrMSG = __('Group message not exists!', 'base');
    }
    
    page_gchat($_POST['profileuser'], $ErrMSG);
    
    die();
}


//===write gmsg && out all===
add_action('wp_ajax_writegmsg',        'writegmsg');
//add_action('wp_ajax_nopriv_writegmsg', 'writegmsg');
function writegmsg(){
    $_POST['profileuser']          = $_POST['profileuser']  ?? '';
    $_POST['gmsg_title']           = $_POST['gmsg_title']   ?? '';
    $_POST['gmsg_txt']             = $_POST['gmsg_txt']     ?? '';
    $_POST['IDListing2']           = $_POST['IDListing2']   ?? 0;
    $_POST['gmsg_reply_parent_id'] = $_POST['gmsg_reply_parent_id'] ?? 0;
    
    $res = write_gmsg($_POST['profileuser'], $_POST['gmsg_title'], $_POST['gmsg_txt'], $_POST['gmsg_reply_parent_id'], $_POST['IDListing2']);
    
    page_gchat($_POST['profileuser'], $res);
    
    die();
}


/* Register a custom post type called "Message group posts". */
function dc_f3_init() {
    $labels = array(
        'name'                  => __( 'Message group posts', 'base' ),
        'singular_name'         => __( 'Message group posts', 'base' ),
        'menu_name'             => __( 'Message group posts', 'base' ),
        'name_admin_bar'        => __( 'Message group posts', 'base' ),
        'add_new'               => __( 'Add New', 'base' ),
        'add_new_item'          => __( 'Add New', 'base' ),
        'new_item'              => __( 'New Message group posts', 'base' ),
        'edit_item'             => __( 'Edit Message group posts', 'base' ),
        'view_item'             => __( 'View Message group posts', 'base' ),
        'all_items'             => __( 'All Message group posts', 'base' ),
        'search_items'          => __( 'Search Message group posts', 'base' ),
        'parent_item_colon'     => __( 'Parent Message group posts:', 'base' ),
        'not_found'             => __( 'No Message group posts found.', 'base' ),
        'not_found_in_trash'    => __( 'No Message group posts found in Trash.', 'base' ),
        'featured_image'        => __( 'Cover image', 'base' ),
        'set_featured_image'    => __( 'Set cover image', 'base' ),
        'remove_featured_image' => __( 'Remove cover image', 'base' ),
        'use_featured_image'    => __( 'Use as cover image', 'base' ),
        'archives'              => __( 'Message group posts archives', 'base' ),
        'insert_into_item'      => __( 'Insert into Message group posts', 'base' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Message group posts', 'base' ),
        'filter_items_list'     => __( 'Filter Message group posts list', 'base' ),
        'items_list_navigation' => __( 'Message group posts list navigation', 'base' ),
        'items_list'            => __( 'Message group posts list', 'base' ),
    );
    
    $args = array(
        'labels'             => $labels,
        'description'        => 'Message group custom post type.',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => false,
        'query_var'          => true,
        'rewrite'            => array( 'slug'=>'gmsg' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 20,
        'supports'           => array( 'title', 'editor' ), //, 'excerpt', 'comments', 'author', 'thumbnail'
        'taxonomies'         => array(),
        'show_in_rest'       => true
    );
    register_post_type( 'gmsg', $args );
    
}
add_action( 'init', 'dc_f3_init' ); ?>