<?php
/*
Template Name: My account [edit]
*/ ?>

<?php if(!is_user_logged_in()){
	wp_redirect(home_url().'/?upenauth');
	exit();
} ?>

<?php get_header() ?>

<?php
	$_GET['user'] = $_GET['user'] ?? 0;
	page_myaccedit($_GET['user']);
?>
	
<?php get_footer() ?>