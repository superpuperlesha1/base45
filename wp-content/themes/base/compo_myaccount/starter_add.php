<?php if (is_user_logged_in()){ ?>
    
    <!-- USER BLOCK 20 min-->
    <div class="modal fade" id="ds_float_userblock_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel8" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?= __('Information', 'base') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body"><?php
                    if( is_user_logged_in() && (isset($_GET['openPayModal']) || (int)get_user_meta(get_current_user_id(), 'admin_payment', true) == 0) ){
                        echo pp_form();
                    } ?>
                </div>
            </div>
        </div>
    </div>

    <!-- USER BLOCK HELLO-->
    <div class="modal fade" id="ds_float_userhello_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?= __('Information', 'base') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body"><?php
                    if(isset($_GET['AdminHello']) && is_user_logged_in()){
                        echo getHelloUser(get_current_user_id());
                    } ?>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal complain to user -->
    <div class="modal fade" id="ds_float_complain_modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabelCTU" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?= __('Complaine to user', 'base') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form action="#uID" class="form" method="POST">
                        <input type="hidden" name="complain_tuUserID" id="complain_tuUserID" value="<?= $_GET['profile'] ?? 0 ?>">
                        <div class="form__row">
                            <div class="form__input-holder">
                                <textarea id="complain_txt" name="complain_txt" maxlength="300"></textarea>
                                <label for="complain_txt"><?= __('Enter complain text', 'base') ?></label>
                            </div>
                        </div>
                        <div id="complain_mssgbox" class="text-danger"></div>
                        <div class="center-holder">
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                            <button type="button" class="btn btn-primary" id="complain_submitteer"><?= __('Send a message to the administrator', 'base') ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal question to admin -->
    <div class="modal fade" id="ds_float_chyear_action_modal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?= __('Change my age', 'base') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form action="#uID" method="POST" class="form">
                        <div class="form__row">
                            <div class="form__input-holder">
                                <textarea name="chyear_txt" id="chyear_txt" class="form-control" maxlength="300" data-err="<?= __('Enter your message at least 6 characters.', 'base') ?>"></textarea>
                                <label for="chyear_txt"><?= __('Type message', 'base') ?></label>
                            </div>
                        </div>
                        <div id="chyear_mssgbox" class="text-danger"></div>
                    </form>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                    <button type="button" class="btn btn-primary" id="chyear_submitteer"><?= __('Send a message to the administrator', 'base') ?></button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal LOGOUT -->
    <div class="modal fade" id="ds_logout_modal" tabindex="-1" role="dialog" aria-labelledby="ds_logout_modalhh" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?= __('Log out', 'base') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                    <a class="btn btn-primary" href="<?= esc_url(get_the_permalink(get_field('sopt_logout_pid', 'option'))) ?>"><?= __('Log out', 'base') ?></a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal AFTER PAYMENT -->
    <div class="modal fade" id="ds_float_afterpayment_modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabelCTU" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?= __('Information', 'base') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <?= (isset($_GET['afterpayment']) ?get_field('sopt_payment_thanks', 'option') :'') ?>
                </div>
            </div>
        </div>
    </div>

    <!-- USER ADMIN BLOCK -->
    <div class="modal fade" id="ds_float_useradminblock_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel10" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?= __('Admin Blocked user', 'base') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div id="ds_float_useradminblock_modal_box" class="modal-body">
                    <?= get_field('sopt_ntf_txt_adminblockuser', 'option') ?>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal MESSAGE -->
    <div class="modal fade" id="ds_float_message_modal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel3" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?= __('Message', 'base') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="ds_float_message_modal_box" class="modal-body"></div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                </div>
            </div>
        </div>
    </div>
<?php }else{ ?>
    <!-- Modal LOGIN -->
    <div class="modal fade" id="ds_float_login_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?= __('Site LOGO', 'base') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><?= __('Login', 'base') ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><?= __('Restore', 'base') ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false"><?= __('Register', 'base') ?></a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <h3><?= __('Login User', 'base') ?></h3>
                            <div class="inline-holder">
                                <a href="<?= auth_google_url ?>" aria-hidden="true" class="btn btn-primary" title="<?= __('Login', 'base') ?>"><?= __('Log in with G+', 'base') ?></a>
                                <a href="<?= auth_fb_url ?>"     aria-hidden="true" class="btn btn-primary" title="<?= __('Login', 'base') ?>"><?= __('Log in with FB', 'base') ?></a>
                                <a href="<?= auth_fb_url ?>"     aria-hidden="true" class="btn btn-primary" title="<?= __('Login', 'base') ?>"><?= __('Log in with TW', 'base') ?></a>
                            </div>
                            <form action="#uID" method="POST" class="form form-login">
                                <div class="form-group">
                                    <?= (isset($_GET['usersecretreg']) ? '<h3>' . $_GET['usersecretreg'] . '</h3>' : '') ?>
                                    <?= (isset($_GET['RestorePass']) ? '<h3>Please enter you new password!</h3>' : '') ?>
                                </div>
                                <div class="form__row">
                                    <div class="form__input-holder">
                                        <input type="email" id="usr_login_email" name="usr_login_email">
                                        <label for="usr_login_email"><?= __('E-Mail', 'base') ?></label>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="form__input-holder">
                                        <input type="password" id="usr_login_pass" name="usr_login_pass">
                                        <label for="usr_login_pass"><?= __('Password', 'base') ?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="usr_login_mssgbox" class="text-danger"></div>
                                </div>
                                <div class="form-group">
                                    <a href="#uID" id="usr_login_start" class="btn btn-primary"><?= __('Login', 'base') ?></a>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade popup-restore" id="profile" role="tabpanel"
                             aria-labelledby="profile-tab">
                            <h3><?= __('Reset your password', 'base') ?></h3>
                            <p><?= __('Enter the email associated with the account to recover your password', 'base') ?></p>
                            <form class="form" action="#" method="POST">
                                <div class="form__row">
                                    <div class="form__input-holder">
                                        <input type="email" id="usr_restore_email" name="usr_restore_email" class="w-100">
                                        <label for="usr_restore_email"><?= __('E-Mail', 'base') ?></label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div id="usr_restore_mssgbox" class="text-danger"></div>
                                </div>
                                <div class="form-group">
                                    <a href="#uID" id="usr_restore_start"
                                       class="btn btn-primary"><?= __('Restore password', 'base') ?></a>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                            <h3><?= __('Create your account', 'base') ?></h3>
                            <?= (isset($GLOBALS['SNR_openreg']['error']) ? '<div class="text-danger">' . $GLOBALS['SNR_openreg']['error'] . '</div>' : '') ?>
                            <div class="dc_soc_in text-center social-registration">
                                <a href="<?= auth_google_url ?>" class="fa fa-google" aria-hidden="true" title="<?= __('Register', 'base') ?>"></a>
                                <a href="<?= auth_fb_url ?>" class="fa fa-facebook" aria-hidden="true" title="<?= __('Register', 'base') ?>"></a>
                                <a href="<?= auth_fb_url ?>" class="fa fa-twitter" aria-hidden="true" title="<?=  __('Register', 'base') ?>"></a>
                            </div>
                            <form action="#uID" method="POST" class="form form-register">

                                <div class="form__row">
                                    <div class="form__input-holder">
                                        <input type="email" id="usr_register_usr_email" name="usr_email" class="form-control" value="<?= (isset($_GET['SNR_openreg_email']) ? $_GET['SNR_openreg_email'] : '') ?>">
                                        <label for="usr_register_usr_email"><?= __('E-Mail', 'base') ?></label>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="form__input-holder">
                                        <input type="text" id="usr_register_usr_zip" name="usr_register_usr_zip" class="form-control" placeholder="">
                                        <label for="usr_register_usr_zip"><?= __('Main Address', 'base') ?></label>
                                        <div id="usr_zip_res"></div>
                                        <input type="hidden" id="usr_register_usr_zip_loc" name="usr_register_usr_zip_loc">
                                        <input type="hidden" id="usr_register_usr_zip_lt"  name="usr_register_usr_zip_lt">
                                        <input type="hidden" id="usr_register_usr_zip_ln"  name="usr_register_usr_zip_ln">
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="form__input-holder">
                                        <input type="text" id="usr_register_usr_fname" name="usr_register_usr_fname" value="<?= (isset($_GET['SNR_openreg_first_name']) ? $_GET['SNR_openreg_first_name'] : '') ?>" maxlength="64">
                                        <label for="usr_register_usr_fname"><?= __('First Name', 'base') ?></label>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="form__input-holder">
                                        <input type="text" id="usr_register_usr_lname" name="usr_register_usr_lname" value="<?= (isset($_GET['SNR_openreg_last_name']) ? $_GET['SNR_openreg_last_name'] : '') ?>" maxlength="64">
                                        <label for="usr_register_usr_lname"><?= __('Last Name', 'base') ?></label>
                                    </div>
                                </div>
                                <div class="form__row">
                                    <div class="form__input-holder">
                                        <input type="password" id="usr_register_usr_pass" name="usr_register_usr_pass">
                                        <label for="usr_register_usr_pass"><?= __('Password', 'base') ?></label>
                                    </div>
                                </div>
                                <div class="form-register__row">
                                    <label for="usr_register_usr_iam_1" class="form__label form-login__label">
                                        <input type="checkbox" id="usr_register_usr_iam_1" name="usr_register_usr_iam_1" class="form__label form-login__label">
                                        <?= __('Yes', 'base') ?>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <div id="usr_register_mssgbox" class="text-danger"></div>
                                </div>
                                <a href="#uID" id="usr_register_start" class="btn btn-primary"><?= __('Register', 'base') ?></a>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div><?php
} ?>

<script>
    var avaMaxSize = '<?= avaMaxSize ?>';

    //jQuery(document).ready(function($){
    document.addEventListener('DOMContentLoaded', function(){

        //===INIT MAP TO USER PROFILE VIEW===
        function init1288(){
          if( typeof su_lt !== 'undefined' && typeof su_ln !== 'undefined' && typeof su_loc !== 'undefined' ){
            const myLatLng = { lat:su_lt, lng:su_ln };
            const map = new google.maps.Map(document.getElementById('ds_showusersingl_map_box'), {
              zoom:   18,
              center: myLatLng,
              styles: csmapstyle
            });
            new google.maps.Marker({position:myLatLng, map, title:su_loc});
          }
        }

        //===REGISTER USER autocomplitter init===
        function init124(){
          if(document.getElementById('usr_register_usr_zip') != null ){
            var input = document.getElementById('usr_register_usr_zip');
            var autocomplete = new google.maps.places.Autocomplete(input);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('usr_register_usr_zip_loc').value = place.name;
                document.getElementById('usr_register_usr_zip_lt').value  = place.geometry.location.lat();
                document.getElementById('usr_register_usr_zip_ln').value  = place.geometry.location.lng();
            });
          }
        }
        init124();

        //===MY ACCOUNT autocomplitter init===
        function init122(){
            if( document.getElementById('usr_zip') != null && document.getElementById('usr_ziptrip') != null ){
              var input1 = document.getElementById('usr_zip');
              var autocomplete1 = new google.maps.places.Autocomplete(input1, AutocompleteOpt);
              google.maps.event.addListener(autocomplete1, 'place_changed', function () {
                  var place1 = autocomplete1.getPlace();
                  document.getElementById('usr_zip_loc').value = '';
                  document.getElementById('usr_zip_lt').value  = '';
                  document.getElementById('usr_zip_ln').value  = '';
                  place1.address_components.forEach(function(item, i, arr) {
                      if( item.types.indexOf('postal_code') > -1 ){
                          document.getElementById('usr_zip_loc').value = place1.name;
                          document.getElementById('usr_zip_lt').value  = place1.geometry.location.lat();
                          document.getElementById('usr_zip_ln').value  = place1.geometry.location.lng();
                      }
                  });
              });
            }
        }
        init122();

        //===OPEN REGISTRATION POPUP
        $(document).on('click', '.js-registration-popup', function(event){
            event.preventDefault();
            $('#ds_float_login_modal').modal();
            $('#register-tab').click();
        });

        //===OPEN PAYMENT MODAL===
        $(document).on('click', '#GoToPaymentModal', function(event){
            $('#ds_float_userhello_modal').modal('hide');
            $('#ds_float_userblock_modal').modal('show');
        });

        //===MENU EDIT MY ACCOUNT===
        $(document).on('click', '.menu_myacc_edit', function(event){
            event.preventDefault();
            setURLpage($(this).find('a').attr('href'));
            var userid = $(this).find('a').attr('data-userid');
            $('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
            $.ajax({
                type: 'POST',
                url:  WPajaxURL,
                data: {
                        action: 'myaccedit',
                        userid: userid,
                      },
                success:     function(data, textStatus, XMLHttpRequest) {
                    $('#base_main').html(data);
                    init122();
                    $('#base_main').fadeTo('slow', 1, function(){  });
                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    top.location.href = WPURL;
                }
            });
        });

        //===MENU VIEW MY ACCOUNT===
        $(document).on('click', '.dc_myacc_view', function(event){
            event.preventDefault();
            setURLpage($(this).attr('href'));
            var userid = $(this).attr('data-userid');
            $('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
            $.ajax({
                type: 'POST',
                url:  WPajaxURL,
                data: {
                        action: 'myaccview',
                        userid: userid,
                      },
                success:     function(data, textStatus, XMLHttpRequest) {
                    $('#base_main').html(data);
                    init1288();
                    $('#base_main').fadeTo('slow', 1, function(){  });
                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    top.location.href = WPURL;
                }
            });
        });

        //===LOGOUT MODAL===
        $(document).on('click', '.menu_logout', function(event){
            event.preventDefault();
            $('#ds_logout_modal').modal('show');
        });

        //===UPDATE MY ACCOUNT AVA===
        $(document).on('input', '#usr_ava', function(event){
            var fileName  = event.target.files[0].name;
            var fileSize  = parseInt(event.target.files[0].size);
            var arr       = ['', '.jpg', '.jpeg', '.png'];
            var pposs     = fileName.indexOf('.');
            var fext      = fileName.substring(pposs);

            if(fileSize < avaMaxSize){
                if(arr.indexOf(fext)){
                    $('#usr_myacupdateava_err').removeClass('text-danger').removeClass('text-success');
                    $('#usr_myacupdateava_err').html(Loading);
                    var form_data = new FormData(document.getElementById('usr_myacupdateava_form'));
                    $.ajax({
                        type:        'POST',
                        url:         WPajaxURL,
                        data:        form_data,
                        contentType: false,
                        processData: false,
                        enctype:     'multipart/form-data',
                        success:     function(data, textStatus, XMLHttpRequest) {
                            data = JSON.parse(data);
                            $('#usr_myacupdateava_err').addClass('text-success');
                            $('#usr_myacupdateava_err').html(data.txt);
                            if(data.ava.length > 0){
                                $('#mainavaacc').attr('src', data.ava);
                                $('#mainavampf').attr('src', data.avab);
                            }
                        },
                        error: function(MLHttpRequest, textStatus, errorThrown) {
                            top.location.href = WPURL;
                        }
                    });
                }else{
                    $('#usr_myacupdateava_err').addClass('text-danger');
                    $('#usr_myacupdateava_err').html($(this).attr('data-error'));
                }
            }else{
                $('#usr_myacupdateava_err').addClass('text-danger');
                $('#usr_myacupdateava_err').html($(this).attr('data-filesizeerr'));
            }
        });

        //===INIT page without AJAX===
        document.addEventListener('DOMContentLoaded', function(){
            //===START INIT===
            init1288();
        });

        //===UPDATE MY ACCOUNT FORM===
        $(document).on('click', '#usr_myacupdate_start', function(){
            event.preventDefault();
            $('#usr_myacupdate_mssgbox').removeClass('text-success').removeClass('text-danger');
            $('#usr_myacupdate_mssgbox').html(Loading);
            var err = '';

            $('#usr_fname').removeClass('border-danger');
            $('#usr_lname').removeClass('border-danger');
            $('#usr_zip').removeClass('border-danger');

            var usr_fname   = $('#usr_fname').val();
            var usr_lname   = $('#usr_lname').val();
            var usr_zip_loc = $('#usr_zip_loc').val();
            var usr_zip_lt  = $('#usr_zip_lt').val();
            var usr_zip_ln  = $('#usr_zip_ln').val();
            
            //===ZIP stable===
            //if(usr_zip_loc.length < 3){
                //err='Enter a more detailed Address.';
                //$('#usr_zip').addClass('border-danger');
            //}
            //if(usr_zip_lt.length < 3){
                //err='Enter a more detailed Address.';
                //$('#usr_zip').addClass('border-danger');
            //}
            //if(usr_zip_ln.length < 3){
                //err='Enter a more detailed Address.';
                //$('#usr_zip').addClass('border-danger');
            //}
            if(usr_fname.length   < 3){
                err='Please enter your "First Name".';
                $('#usr_fname').addClass('border-danger');
            }
            if(usr_lname.length   < 3){
                err='Please enter your "Last Name"';
                $('#usr_lname').addClass('border-danger');
            }

            var form_data = new FormData(document.getElementById('usr_myacupdate_form'));
            if(err==''){
                $.ajax({
                    type:        'POST',
                    url:         WPajaxURL,
                    data:        form_data,
                    contentType: false,
                    processData: false,
                    success:     function(data, textStatus, XMLHttpRequest) {
                        data = JSON.parse(data);
                        $('#usr_myacupdate_mssgbox').addClass('text-success');
                        $('#usr_myacupdate_mssgbox').html(data.txt);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                $('#usr_myacupdate_mssgbox').addClass('text-danger');
                $('#usr_myacupdate_mssgbox').html(err);
            }
        });


        //===BLOCK USER===
        $(document).on('click', '#admin_block_user', function(){
            var usrid = $(this).attr('data-userid');
            $.ajax({
                type: 'POST',
                url:  WPajaxURL,
                data: {
                        action: 'blockuser',
                        usrid:  usrid,
                      },
                success:     function(data, textStatus, XMLHttpRequest) {

                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    top.location.href = WPURL;
                }
            });
        });


        //===PAYMENT USER===
        $(document).on('click', '#admin_payment_user', function(){
            var usrid = $(this).attr('data-userid');
            $.ajax({
                type: 'POST',
                url:  WPajaxURL,
                data: {
                        action: 'paymentuser',
                        usrid:  usrid,
                      },
                success:     function(data, textStatus, XMLHttpRequest) {

                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    top.location.href = WPURL;
                }
            });
        });


        //===CHANGE MY CONFIRMATION SETTINGS===
        $(document).on('click', '#usr_cf_start', function(){
            event.preventDefault();
            $('#usr_ch_confsets_msgbox').html(Loading);
            var err = '';

            if($('#usr_cf_1').is(':checked')){var usr_cf_1 = 1;}else{var usr_cf_1 = 0;}
            if($('#usr_cf_2').is(':checked')){var usr_cf_2 = 1;}else{var usr_cf_2 = 0;}
            if($('#usr_cf_3').is(':checked')){var usr_cf_3 = 1;}else{var usr_cf_3 = 0;}
            if($('#usr_cf_4').is(':checked')){var usr_cf_4 = 1;}else{var usr_cf_4 = 0;}
            var usr_cf_uid = $('#'+usr_cf_uid).val();

            if(err==''){
                $.ajax({
                    type: 'POST',
                    url:  WPajaxURL,
                    data: {
                            action:     'chconfirmation',
                            usr_cf_1:   usr_cf_1,
                            usr_cf_2:   usr_cf_2,
                            usr_cf_3:   usr_cf_3,
                            usr_cf_4:   usr_cf_4,
                            usr_cf_uid: usr_cf_uid,
                          },
                    success:     function(data, textStatus, XMLHttpRequest) {
                        data = JSON.parse(data);
                        $('#usr_ch_confsets_msgbox').html(data.err);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                $('#usr_ch_confsets_msgbox').html(err);
            }
        });

        //===CHANGE MY PASSWORD===
        $(document).on('click', '#usr_ch_pass_start', function(){
            event.preventDefault();
            $('#usr_ch_pass_msgbox').html(Loading);
            var err = '';

            var usr_old_pass    = $('#usr_old_pass').val();
            var usr_ch_pass     = $('#usr_ch_pass').val();
            var usr_ch_passc    = $('#usr_ch_passc').val();
            var usr_ch_pass_uid = $('#usr_ch_pass_uid').val();

            if(usr_ch_pass.length  < 3){err='Please enter your "New password"';}
            if( usr_ch_pass.indexOf(' ') != -1 ){err='Password cannot contain a space';}
            if(usr_ch_pass != usr_ch_passc){err='Password mismatch';}

            if(err==''){
                $.ajax({
                    type: 'POST',
                    url:  WPajaxURL,
                    data: {
                            action:          'chpass',
                            usr_old_pass:    usr_old_pass,
                            usr_ch_pass:     usr_ch_pass,
                            usr_ch_pass_uid: usr_ch_pass_uid,
                          },
                    success:     function(data, textStatus, XMLHttpRequest) {
                        $('#usr_ch_pass_msgbox').html(data);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                $('#usr_ch_pass_msgbox').html(err);
            }
        });


        $(document).on('click', '#usr_restore_start', function(){
            $('#usr_restore_mssgbox').html(Loading);
            var err = '';

            var usr_restore_email = $('#usr_restore_email').val();

            if(usr_restore_email.length < 6){err='Please enter your "E-Mail"';}

            if(err==''){
                $.ajax({
                    type: 'POST',
                    url:  WPajaxURL,
                    data: {
                            action:            'restpass',
                            usr_restore_email: usr_restore_email,
                          },
                    success:     function(data, textStatus, XMLHttpRequest) {
                        $('#usr_restore_mssgbox').html(data);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                $('#usr_restore_mssgbox').html(err);
            }
        });


        //===LOGIN FORM===
        $(document).on('click', '#usr_login_start', function(event){
            event.preventDefault();
            event.stopPropagation();
            $('#usr_login_mssgbox').html(Loading);
            var err = '';
            var usr_login_email = $('#usr_login_email').val();
            var usr_login_pass  = $('#usr_login_pass').val();

            if(usr_login_email.length < 6){err='Please enter your "E-Mail"';}
            if(usr_login_pass.length  < 3){err='Please enter your "Password"';}

            if(err==''){
                $.ajax({
                    type: 'POST',
                    url:  WPajaxURL,
                    data: {
                            action:           'authusr',
                            usr_login_email:  usr_login_email,
                            usr_login_pass:   usr_login_pass,
                          },
                    success:     function(data, textStatus, XMLHttpRequest) {
                        data = JSON.parse(data);
                        if(data.err.length == 0){
                            top.location.href = eventsmyurl;
                        }else{
                            $('#usr_login_mssgbox').html(data.err);
                        }
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                $('#usr_login_mssgbox').html(err);
            }
        });


        //===REGISTER FORM===
        $(document).on('click', '#usr_register_start', function(){
            $('#usr_register_mssgbox').html(Loading);
            var err = '';

            $('#usr_register_usr_year').removeClass('border-danger');
            $('#usr_register_usr_zip_loc').removeClass('border-danger');
            $('#usr_register_usr_pass').removeClass('border-danger');
            $('#usr_register_usr_fname').removeClass('border-danger');
            $('#usr_register_usr_lname').removeClass('border-danger');
            $('#usr_register_usr_email').removeClass('border-danger');
            $('#usr_register_usr_iam_1').removeClass('border-danger');
            $('#usr_register_usr_iam_2').removeClass('border-danger');
            $('#usr_register_usr_iam_3').removeClass('border-danger');
            $('#usr_register_usr_iam_4').removeClass('border-danger');


            var usr_register_usr_year      = $('#usr_register_usr_year').val();
            var usr_register_usr_zip_loc   = $('#usr_register_usr_zip_loc').val();
            var usr_register_usr_zip_lt    = $('#usr_register_usr_zip_lt').val();
            var usr_register_usr_zip_ln    = $('#usr_register_usr_zip_ln').val();
            var usr_register_usr_pass      = $('#usr_register_usr_pass').val();
            var usr_register_usr_fname     = $('#usr_register_usr_fname').val();
            var usr_register_usr_lname     = $('#usr_register_usr_lname').val();
            var usr_register_usr_email     = $('#usr_register_usr_email').val();
            var usr_register_usr_iam_1     = $('#usr_register_usr_iam_1').is(':checked');
            var usr_register_usr_iam_2     = $('#usr_register_usr_iam_2').is(':checked');
            var usr_register_usr_iam_3     = $('#usr_register_usr_iam_3').is(':checked');
            var usr_register_usr_iam_4     = $('#usr_register_usr_iam_4').is(':checked');


            if(usr_register_usr_zip_loc.length   < 3){
                err='Please enter your "Address"';
                $('#usr_register_usr_zip_loc').addClass('border-danger');
            }
            //if(usr_register_usr_zip_lt.length   < 3){
                //err='Enter a more detailed address.';
                //$('#usr_register_usr_zip_loc').addClass('border-danger');
            //}
            //if(usr_register_usr_zip_ln.length   < 3){
                //err='Enter a more detailed address.';
                //$('#usr_register_usr_zip_loc').addClass('border-danger');
            //}
            //if(usr_register_usr_pass.length  < 3){
                //err='Please enter your "Password"';
                //$('#usr_register_usr_pass').addClass('border-danger');
            //}
            if( usr_register_usr_pass.indexOf(' ') != -1 ){
                err='Password cannot contain a space';
                $('#usr_register_usr_pass').addClass('border-danger');
            }
            if(usr_register_usr_fname.length < 3){
                err='Please enter your "First Name"';
                $('#usr_register_usr_fname').addClass('border-danger');
            }
            if(usr_register_usr_lname.length < 3){
                err='Please enter your "Last Name"';
                $('#usr_register_usr_lname').addClass('border-danger');
            }
            if(usr_register_usr_email.length < 6){
                err='Please enter your "E-Mail field"';
                $('#usr_register_usr_email').addClass('border-danger');
            }
            if(!usr_register_usr_iam_1          ){
                err='Please confirm Yes';
                $('#usr_register_usr_iam_1').addClass('border-danger');
            }
            
            if(err==''){
                $.ajax({
                    type: 'POST',
                    url:  WPajaxURL,
                    data: {
                            action:                     'regusr',
                            usr_register_usr_zip_loc:   usr_register_usr_zip_loc,
                            usr_register_usr_zip_lt:    usr_register_usr_zip_lt,
                            usr_register_usr_zip_ln:    usr_register_usr_zip_ln,
                            usr_register_usr_pass:      usr_register_usr_pass,
                            usr_register_usr_fname:     usr_register_usr_fname,
                            usr_register_usr_lname:     usr_register_usr_lname,
                            usr_register_usr_email:     usr_register_usr_email,
                          },
                    success:     function(data, textStatus, XMLHttpRequest) {
                        $('#usr_register_mssgbox').html(data);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                $('#usr_register_mssgbox').html(err);
            }
        });


        function initialize() {
          var input = document.getElementById('searchTextField');
          new google.maps.places.Autocomplete(input);
        }


        //===set focus for all windows===
        $('.modal').on('shown.bs.modal', function() {
            $('.ds_focusmy').focus();
        });


        

        
        //===AFTER PAYMENT===
        if(afterpayment==1){
            $('#ds_float_afterpayment_modal').modal();
        }


        //===LOGIN FORM===
        if(ConfirmEMail==1 || RestorePass==1 || openregauth==1 || usersecretreg==1 || upenauth==1){
            $('#ds_float_login_modal').modal();
            if(openregauth==1){
                $('#register-tab').click();
            }
        }
        $(document).on('click', '.ds_float_login', function(){
            event.preventDefault();
            $('#ds_float_login_modal').modal();
        });


        //===GO TO PAYMENT===
        $(document).on('click', '#gotopayment', function(){
            $('#ds_float_userblock_modal').modal();
        });
        //===BLOCK USER PAY===
        if(openPayModal==1){
            $('#ds_float_userblock_modal').modal();
        };

        //===ADMIN BLOCK USER===
        if(AdminBlockUser==1){
            $('#ds_float_useradminblock_modal').modal();
        };
        //===HELLO USER===
        if(AdminHello==1){
            $('#ds_float_userhello_modal').modal();
        };


        //===ADD private MESSAGE===
        $(document).on('click', '.dc_add_msg', function(){
            $('#dc_nm_ppid').val( $(this).attr('data-pid') );
        });


        //===complain to user MODAL===
        $(document).on('click', '#complain_action', function(){
            $('#ds_float_complain_modal').modal();
        });
        //===complain to user SUBMIT===
        $(document).on('click', '#complain_submitteer', function(){
            var err = '';

            $('#complain_txt').removeClass('border-danger');
            $('#complain_mssgbox').html(Loading);

            var complain_txt = $('#complain_txt').val();
            var tuUserID     = $('#complain_tuUserID').val();

            if( complain_txt.length < 6 ){
                err='Please enter your "Message"';
                $('#complain_txt').addClass('border-danger');
            }

            if(err.length == 0){
                $.ajax({
                    type: 'POST',
                    url:  WPajaxURL,
                    data: {
                            action:   'complaineuser',
                            message:  complain_txt,
                            tuUserID: tuUserID,
                          },
                    success:     function(data, textStatus, XMLHttpRequest) {
                        $('#complain_mssgbox').html(data);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                $('#complain_mssgbox').html(err);
            }
        });


        //===COMPLAINE MODAL===
        $(document).on('click', '#chyear_action', function(){
            $('#ds_float_chyear_action_modal').modal();
        });
        //===COMPLAINE SUBMIT===
        $(document).on('click', '#chyear_submitteer', function(){
            var err = '';

            $('#chyear_txt').removeClass('border-danger');
            $('#chyear_mssgbox').html(Loading);

            var chyear_txt = $('#chyear_txt').val();
            var tuUserID   = $('#chyear_tuUserID').val();

            if( chyear_txt.length < 6 ){
                err=$('#chyear_txt').attr('data-err');
                $('#chyear_txt').addClass('border-danger');
            }

            if(err.length == 0){
                $.ajax({
                    type: 'POST',
                    url:  WPajaxURL,
                    data: {
                            action:  'chyearuser',
                            message: chyear_txt,
                          },
                    success: function(data, textStatus, XMLHttpRequest) {
                        $('#chyear_mssgbox').html(data);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                $('#chyear_mssgbox').html(err);
            }
        });
    });
</script>