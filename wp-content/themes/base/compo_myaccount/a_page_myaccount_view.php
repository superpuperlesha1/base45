<?php
/*
Template Name: My account [view]
*/ ?>

<?php if(!is_user_logged_in()){
	wp_redirect(home_url().'/?upenauth');
	exit();
} ?>

<?php get_header() ?>

<?php page_myaccview( ($_GET['profile'] ?? 0) ) ?>
	
<?php get_footer() ?>