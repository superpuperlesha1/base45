<?php
/*
Template Name: My account [Logout]
*/ ?>

<?php if( is_user_logged_in() ){
	wp_logout();
	wp_redirect( get_the_permalink(get_field('sopt_logout_pid', 'option')) );
	exit();
} ?>

<?php get_header() ?>

	<section class="container">
        <div class="row">
            <div class="col-md-12">
                <?php while (have_posts()) {
                    the_post();
                    echo '<h1>' . get_the_title() . '</h1>';
                    the_content();
                } ?>
            </div>
        </div>
    </section>

<?php get_footer() ?>