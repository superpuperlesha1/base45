<?php
/**
 * Post template.
 *
 * @package base
 */
?>
<?php get_header() ?>
    
    <section class="container">
        <div class="row">
            <div class="col-md-12">
                <?php while (have_posts()) {
                    the_post();
                    
                    $img       = wp_get_attachment_image_src(get_post_thumbnail_id(get_queried_object_id()), 'thumbnail_300x200');
                    $img       = (isset($img[0]) ?$img[0] :esc_url(get_template_directory_uri()).'/img/noimage.jpg');
                    $image_alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
                    $image_alt = ($image_alt ?htmlspecialchars($image_alt) :htmlspecialchars(get_the_title())); ?>
                    <img src="<?= $img ?>" alt="<?= $image_alt ?>" width="300" height="200"><?php

                    echo '<h1>' . get_the_title() . '</h1>'; ?>
                    <time datetime="<?= get_post_time('Y-m-d g:i', true, get_the_id()) ?>"><?= get_post_time(get_option('date_format'), true, get_the_id()) ?></time><?php
                    the_content();

                    if(comments_open() || get_comments_number()){
						comments_template();
                    }
                } ?>
            </div>
        </div>
    </section>

<?php get_footer() ?>