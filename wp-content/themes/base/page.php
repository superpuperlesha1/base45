<?php
/**
 * Page template.
 *
 * @package base
*/
?>
<?php get_header() ?>

    <section class="container">
        <div class="row">
            <div class="col-md-12">
                <?php while (have_posts()) {
                    the_post();
                    echo '<h1>' . get_the_title() . '</h1>';
					//echo woocommerce_breadcrumb();
                    the_content();
                } ?>
            </div>
        </div>
    </section>
    
<?php get_footer() ?>