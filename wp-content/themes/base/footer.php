<?php
/**
* The footer.
*
* @package base
*/
?>
        </main>

        <footer class="container mt-2">
            <div class="row bg-light">
                <div class="col-md-3">
                    <a href="<?= esc_url(home_url()) ?>">
                        <!--<img src="<?= esc_url(get_template_directory_uri()).'/img/logo.png' ?>" alt="<?= htmlspecialchars(get_bloginfo('name')) ?>" class="img-fluid">-->
                        <svg width="50" height="50"><circle cx="25" cy="25" r="25"/></svg>
                    </a>
                </div>
                <div class="col-md-9">
                    <nav class="navbar"><?php
            			wp_nav_menu([
                            'theme_location'  => 'FootMenu',
                            'menu'            => '', 
                            'container'       => '', 
                            'container_class' => '', 
                            'container_id'    => '',
                            'menu_class'      => 'nav justify-content-center',
                            'menu_id'         => 'FMid1',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'before'          => '',
                            'after'           => '',
                            'link_before'     => '',
                            'link_after'      => '',
                            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'depth'           => 0,
                            'walker'          => new BT5_Menu(),
                        ]) ?>
                    </nav>
                </div>
            </div>
        </footer>


        <div id="CMSScripts">
            <script>
                var WPURL            = '<?= esc_url(home_url()) ?>';
                var WPajaxURL        = '<?= admin_url('admin-ajax.php') ?>';
                var ConfirmEMail     = <?= (isset($ConfirmEMail)           ?1 :0) ?>;
                var RestorePass      = <?= (isset($_GET['RestorePass'])    ?1 :0) ?>;
                var openPayModal     = <?= (isset($_GET['openPayModal'])   ?1 :0) ?>;
                var AdminBlockUser   = <?= (isset($_GET['AdminBlockUser']) ?1 :0) ?>;
                var AdminHello       = <?= (isset($_GET['AdminHello'])     ?1 :0) ?>;
                var usersecretreg    = <?= (isset($_GET['usersecretreg'])  ?1 :0) ?>;
                var upenauth         = <?= (isset($_GET['upenauth'])       ?1 :0) ?>;
                var afterpayment     = <?= (isset($_GET['afterpayment'])   ?1 :0) ?>;
                var openregauth      = <?= (isset($GLOBALS['SNR_openreg']) || isset($_GET['SNR_openreg_email']) ?1 :0) ?>;
                var CheckTimeEvents  = <?= (is_user_logged_in() ?(int)get_field('sopt_ajax_check_sec', 'option')*1000 :0) ?>;
                var sopt_strype_key  = '<?= get_field('sopt_strype_key', 'option') ?>';
                var eventsmyurl      = '<?= get_permalink(get_field('sopt_events_pid', 'option')) ?>';
                var AutocompleteOpt  = {componentRestrictions: {country: [<?php //echo(current_user_can('administrator') ?'' :'"us", "ca"') ?>]}};
            </script>

            <!--<?php if( get_field('wmns_sqookie_yn', 'option') && !isset($_COOKIE['wmns_sqookie_setup']) ){ ?>
                <div id="dialogbox">
                    <div>
                        <div id="dialogboxbody">
                            <?php echo str_replace(array("\r", "\n"), '', apply_filters('the_content',  get_field('wmns_sqookie_content', 'option'))) ?>
                        </div>
                        <div id="dialogboxfoot">
                            <button id="dialogAccept" type="button"><?php _e('Accept', 'base') ?></button>
                            <button id="dialogDecline" type="button"><?php _e('Decline', 'base') ?></button>
                        </div>
                    </div>
                </div>
                <script>
                    dialogAccept.onclick = function() {
                        dialogbox.style.display = "none";
                        document.cookie         = "wmns_sqookie_setup=1; max-age=<?php echo (int)get_field('wmns_sqookie_timesec', 'option') ?>;";
                        acceptCookies           = true;
                        console.log('Qookie setuping.');
                    }
                </script><?php
            } ?>-->
            
    	    <?php wp_footer() ?>

            <?php echo get_num_queries(); ?> queries in <?php timer_stop(1); ?> seconds.
        </div>

    </body>
</html>