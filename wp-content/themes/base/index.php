<?php
/**
 * Main loop template.
 *
 * @package wpdevtest
*/
?>
<?php get_header() ?>

  <section class="container">
    <div class="row">
        <div class="col-md-12">
          <h1><?= (get_option('page_for_posts') ?get_the_title(get_option('page_for_posts')) :__('Blog', 'base')) ?></h1>
        </div>    
    </div>
    <?php if(have_posts()){
        while(have_posts()){
          the_post();
          get_template_part('template_part/list_item_post');
        } ?>
    <?php get_template_part('template_part/block_pagination');
    }else{
          get_template_part('template_part/not_found');
    } ?>
  </section>

<?php get_footer() ?>