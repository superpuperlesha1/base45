<?php

echo'<style>
		.c1{
				background-size: cover;
				width:30px;
				height:30px;
			}
	</style>
	<script>
		document.addEventListener("DOMContentLoaded", function(event){
				var filesToUpload  = [];

			    $(document).on(\'change\', \'#ybop_file\', function (evt){
			    	//===select files===
					var arrExt         = '.json_encode($GLOBALS['arrExt']).';
					var arrExtImg      = '.json_encode($GLOBALS['arrExtImg']).';
					var FSizeM         = '.$GLOBALS['FSizeM'].';
					var RImgDef        = "'.esc_url(get_stylesheet_directory_uri()).'/img/img.png";
					var ybop_file_errs = document.getElementById("ybop_file_errs");
			    	ybop_file_errs.innerHTML = "";
			        for(var i=0; i<evt.target.files.length; i++){
			            var file    = evt.target.files[i];
			            var fileT   = evt.target.files[i].type;
			            var fileN   = evt.target.files[i].name;
			            var fileS   = evt.target.files[i].size;
			            var fileId  = "file_" + evt.target.files[i].size.toString() + evt.target.files[i].lastModified.toString();
			            var fileURL = URL.createObjectURL(evt.target.files[i]);

			            if( fileS>FSizeM ){
			            	ybop_file_errs.append("'.__('File size no more: ', 'um-theme').'"+FSizeM+" Bytes. ("+fileN+")");
			            	continue;
			            }
		            	
		            	if( arrExt.indexOf(fileT)==-1 ){
		            		ybop_file_errs.append("'.__('File type banned:', 'um-theme').' ["+fileT+"]. ");
		            		continue;
		            	}

	            		filesToUpload.push({id:fileId, file:file});

			            RImg = (arrExtImg.indexOf(fileT)==-1 ?RImgDef :fileURL);
			            
						$("<li class=\'ybop_file_del c5\' data-fileid=\'"+fileId+"\'><div  class=\'file-info\'><div class=\'c1\' style=\"background-image:url(\'"+RImg+"\');\"></div><div>"+fileN+"</div></div><i class=\'c3 ybop_file_del_btn\'>x</i></li>").insertAfter(\'#ybop_file_table_1\');
			        };
					evt.target.value = null;
			    });
			    

			    $(document).on("click", ".ybop_file_del_btn", function(){
					var fileId = $(this).parent(".ybop_file_del").attr("data-fileid");
					for( var i=0; i<filesToUpload.length; ++i ){
						if(filesToUpload[i].id === fileId){
					    	filesToUpload.splice(i, 1); // delete a file from list.
					    	$(this).parent(".ybop_file_del").remove(); // remove file from view .
						}
					}
				});


				var frm = document.getElementById("ybop_form");
				if(frm != null){
					frm.addEventListener("submit", sbScript);
				}
				function sbScript(event){
					event.stopPropagation();
					event.preventDefault();
					var form_data = new FormData(frm);
					ybop_file_errs.innerHTML = "";
					
					for(var i=0, len=filesToUpload.length; i<len; i++){
						form_data.append("ybop_file[]", filesToUpload[i].file);
			        }
			        
					$.ajax({
						type:        "POST",
						url:         WPajaxURL,
						data:        form_data,
						contentType: false,
		        		processData: false,
						success:     function(data, textStatus, XMLHttpRequest) {
							data = JSON.parse(data);
							//if(data.err.length){
								ybop_file_errs.innerHTML = data.err;
							//}else{
								//top.location.reload();
							//}
						},
						error: function(MLHttpRequest, textStatus, errorThrown) {
							alert("System error!");
							top.location.href = WPURL;
						}
					});
				};


				//===MENU OPEN UPLOADER===
				$(document).on("click", ".w51p_menu_uploader a", function(event){
					event.stopPropagation();
					event.preventDefault();
					setURLpage($(this).attr("href"));
					$("#base_main").fadeTo(fadeSpeed, 0, "linear", function(){  });
					$.ajax({
						type: "POST",
						url:  WPajaxURL,
						data: {
								action: "getuploaderajax",
							  },
						success: function(data, textStatus, XMLHttpRequest) {
							$("#base_main").html(data);
							$("#base_main").fadeTo("slow", 1, function(){  });
						},
						error: function(MLHttpRequest, textStatus, errorThrown) {
							alert("System error!");
							top.location.href = WPURL;
						}
					});
				});

			
		});
	</script>';