<script>
    document.addEventListener('DOMContentLoaded', function(){

        var notifyRead       = '<?= notifyRead ?>';
        var notifyURead      = '<?= notifyURead ?>';

        //===MENU notifys===
        function w51p_menu_notifys(pg='', unread=''){
            $('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
            $.ajax({
                type:   'POST',
                url:    WPajaxURL,
                data:{
                        action: 'menunotifys',
                        pg:     pg,
                        unread: unread
                     },
                success:     function(data, textStatus, XMLHttpRequest) {
                    $('#base_main').html(data);
                    $('#base_main').fadeTo('slow', 1, function(){  });
                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    top.location.href = WPURL;
                }
            });
        }

        //===MENU page notifys unreaded===
        $(document).on('click', '.menu_notices, #w51p_menu_notifys_ur', function(event){
            event.preventDefault();
            setURLpage($(this).find('a').attr('href'));
            w51p_menu_notifys(1, 0);
        });

        //===MENU page notifys readed===
        $(document).on('click', '#w51p_menu_notifys_rd', function(event){
            event.preventDefault();
            setURLpage($(this).find('a').attr('href'));
            w51p_menu_notifys(1, 1);
        });

        //===PAGING page notifys===
        $(document).on('click', '.paging_notifys_rd', function(event){
            event.preventDefault();
            setURLpage($(this).attr('href'));
            var pg = $(this).attr('data-pg');
            w51p_menu_notifys(pg, 1);
        });

        //===PAGING page notifys===
        $(document).on('click', '.paging_notifys_ur', function(event){
            event.preventDefault();
            setURLpage($(this).attr('href'));
            var pg = $(this).attr('data-pg');
            w51p_menu_notifys(pg, 0);
        });


        //===OPEN NOTIFICATION===
        $(document).on('click', '.dc_notyfication_openclose', function(){
            var notyid = $(this).attr('data-notyid');
            var itemid = $(this).attr('id');
            var readCount   = $('#w51p_menu_notifys_rd_count').html();
            var unreadCount = $('#w51p_menu_notifys_ur_count').html();
            $(this).removeClass('fa-envelope-open-o').removeClass('fa-envelope-o');
            $.ajax({
                type:   'POST',
                url:    WPajaxURL,
                data:{
                        action: 'notyopenclose',
                        notyid: notyid,
                     },
                success: function(data, textStatus, XMLHttpRequest) {
                    if(data=='1'){
                        readCount++;
                        unreadCount--;
                        $('#'+itemid).attr('src', notifyRead);
                    }else{
                        readCount--;
                        unreadCount++;
                        $('#'+itemid).attr('src', notifyURead);
                    }

                    $('#w51p_menu_notifys_rd_count').html(readCount);
                    $('#w51p_menu_notifys_ur_count').html(unreadCount);

                    siteNotyfy(unreadCount);
                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    top.location.href = WPURL;
                }
            });
        });


        //===NOTIFY SELECT ALL===
        $(document).on('click', '#dc_notify_selall', function(event){
            if( $(this).is(':checked') ){
                $('.wd_notify_id').prop("checked", true);
            }else{
                $('.wd_notify_id').prop("checked", false);
            }
        });


        //===DELETE SINGLE NOTIFY===
        $(document).on('click', '.dc_notification_delete', function(){
            $('#ds_delnotify_modal').modal('show');
            var notifyid = $(this).attr('data-notifyid');
            var unread   = $(this).attr('data-unread');
            $('#dc_notification_delete_y').attr('data-notifyid', notifyid);
            $('#dc_notification_delete_y').attr('data-unread',   unread);
        });
        $(document).on('click', '#dc_notification_delete_y', function(){
            var notifyid = $(this).attr('data-notifyid');
            var unread   = $(this).attr('data-unread');
            $('#ds_delnotify_modal').modal('hide');
            $('.modal-backdrop.fade.show').remove();
            $('#base_main').html(Loading);
            $.ajax({
                    type:   'POST',
                    url:    WPajaxURL,
                    data:{
                            action:   'delnotify',
                            notifyid: notifyid,
                            unread:   unread
                         },
                    success: function(data, textStatus, XMLHttpRequest) {
                        $('#base_main').html(data);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
        });

        //===delete notify group===
        $(document).on('click', '#wd_notify_dellerarr', function(){
            $('#ds_delnotifys_modal').modal('show');
            $('#wd_notify_dellerarr_y_err').html('');
            var readed = $(this).attr('data-read');
            var err    = $(this).attr('data-errmsg');
            $('#wd_notify_dellerarr_y').attr('data-read',   readed);
            $('#wd_notify_dellerarr_y').attr('data-errmsg', err);
        });
        $(document).on('click', '#wd_notify_dellerarr_y', function(){
            var readed = $(this).attr('data-read');
            var msgarr = [];
            $('.wd_notify_id:checked').each(function(){
                msgarr.push($(this).val());
            });
            if(msgarr.length){
                $('#ds_delnotifys_modal').modal('hide');
                $('#base_main').html(Loading);
                $.ajax({
                    type: 'POST',
                    url:  WPajaxURL,
                    data:{
                            action:'deletenotifyarr',
                            msgarr: msgarr,
                            readed: readed,
                         },
                    success: function(data, textStatus, XMLHttpRequest) {
                        $('#base_main').html(data);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                $('#wd_notify_dellerarr_y_err').html($(this).attr('data-errmsg'));
            }
        });

        //===unread notify group===
        $(document).on('click', '#wd_notify_unreadarr', function(){
            var readed = $(this).attr('data-read');
            var msgarr = [];
            $('.wd_notify_id:checked').each(function(){
                msgarr.push($(this).val());
            });

            if(msgarr.length){
                $('#base_main').html(Loading);
                $.ajax({
                    type: 'POST',
                    url:  WPajaxURL,
                    data:{
                            action:'unreadnotifyarr',
                            msgarr: msgarr,
                            readed: readed,
                         },
                    success: function(data, textStatus, XMLHttpRequest) {
                        $('#base_main').html(data);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                alert($(this).attr('data-errmsg'));
            }
        });

        //===read notify group===
        $(document).on('click', '#wd_notify_readarr', function(){
            var readed = $(this).attr('data-read');
            var msgarr = [];
            $('.wd_notify_id:checked').each(function(){
                msgarr.push($(this).val());
            });

            if(msgarr.length){
                $('#base_main').html(Loading);
                $.ajax({
                    type: 'POST',
                    url:  WPajaxURL,
                    data:{
                            action:'readnotifyarr',
                            msgarr: msgarr,
                         },
                    success: function(data, textStatus, XMLHttpRequest) {
                        $('#base_main').html(data);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        top.location.href = WPURL;
                    }
                });
            }else{
                alert($(this).attr('data-errmsg'));
            }
        });

        function siteNotyfy(state=0){
            if(state>0){
                $('.menu_notices').addClass('navigation__link--active');
            }else{
                $('.menu_notices').removeClass('navigation__link--active');
            }
        }

    });
</script>

<!-- Modal DELETE GROUP NOTYFYS -->
<div class="modal fade" id="ds_delnotifys_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Delete notifications', 'base') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="wd_notify_dellerarr_y_err"></div>
                <div class="center-holder">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                    <button id="wd_notify_dellerarr_y" class="btn btn-primary" data-read="" data-errmsg=""><?= __('Delete', 'base') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal DELETE SINGLE NOTYFY -->
<div class="modal fade" id="ds_delnotify_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Delete notification', 'base') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p><?= __('Are you sure that you want to do this?', 'base') ?></p>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?= __('Close', 'base') ?></button>
                <button id="dc_notification_delete_y" class="btn btn-primary" data-unread="" data-notifyid=""><?= __('Delete', 'base') ?></button>
            </div>
        </div>
    </div>
</div>