<?php

define('notifyRead',     esc_url(get_template_directory_uri()).'/compo_notifys/img/notify_r.svg');
define('notifyURead',    esc_url(get_template_directory_uri()).'/compo_notifys/img/notify_u.svg');

/*$id = get_field('sopt_notification_pid', 'option') ?>
<li class="menu_notices <?= (getNotifyUnReadedCount(get_current_user_id())>0 ?'active' :'') ?>">
	<a href="<?= esc_url(get_the_permalink($id)) ?>/?unread" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
</li>*/

//===PAGE notifys===
add_action('wp_ajax_menunotifys',        'menunotifys');
//add_action('wp_ajax_nopriv_menunotifys', 'menunotifys');
function menunotifys(){
	$_POST['pg']     = $_POST['pg']     ?? '';
	$_POST['unread'] = $_POST['unread'] ?? '';
	page_notifys($_POST['pg'], $_POST['unread']);
	die();
}


add_action( 'wp_footer', 'action_function_name_2474' );
function action_function_name_2474(){
    get_template_part('/compo_notifys/starter_add');
}


//===PAGE notifys===
function page_notifys($pg = 1, $unread = 1){
    //===CHECK PAYMENT===
    $checkUserCS = checkUserCS(true);
    if($checkUserCS){
        echo $checkUserCS;
        exit();
    }

    //===VALIDATION===
    $pg = (int)$pg;
    if ($pg < 1) {
        $pg = 1;
    }


    $unread = (int)$unread;
    if($unread < 0) {
        $unread = 0;
    }

    $rposts   = getNotifyReaded();
    $unrposts = getNotifyUnReaded(); ?>

    <section class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?= get_the_title(get_field('sopt_notification_pid', 'option')) ?></h1>
                <span id="w51p_menu_notifys_ur">
                    <a href="<?= get_the_permalink(get_field('sopt_notification_pid', 'option')) ?>?unread" class="badge badge-secondary <?= ($unread ?'badge-secondary' :'badge-success') ?>"><?= __('Unread Notification', 'base') ?>&nbsp;
                        <span>(<span  id="w51p_menu_notifys_ur_count"><?= count($unrposts) ?></span>)</span>
                    </a>
                </span>
                <span id="w51p_menu_notifys_rd">
                    <a href="<?= get_the_permalink(get_field('sopt_notification_pid', 'option')) ?>?read"   class="badge badge-secondary <?= ($unread ?'badge-success' :'badge-secondary') ?>"><?= __('Read Notification', 'base') ?>&nbsp;
                        <span> (<span id="w51p_menu_notifys_rd_count"><?= count($rposts) ?></span>)</span>
                    </a>
                </span>
            </div>
            <div class="col-md-12 mt-2">
                <label class="form__label form-login__label" for="dc_notify_selall">
                    <input type="checkbox" id="dc_notify_selall">
                    <?= __('Select all', 'base') ?>
                </label>
                <button id="wd_notify_readarr"   class="btn btn-primary" data-read="<?= $unread ?>" data-errmsg="<?= __('Select messages!', 'base') ?>"><?= __('Mark as read', 'base') ?></button>
                <button id="wd_notify_unreadarr" class="btn btn-primary" data-read="<?= $unread ?>" data-errmsg="<?= __('Select messages!', 'base') ?>"><?= __('Mark as unread', 'base') ?></button>
                <button id="wd_notify_dellerarr" class="btn btn-primary" data-read="<?= $unread ?>" data-errmsg="<?= __('Select messages!', 'base') ?>"><?= __('Delete', 'base') ?></button>
            </div>

            <div class="col-md-12 mt-2"><?php
                if ($unread == 0) {
                    $posts = &$unrposts;
                } else {
                    $posts = &$rposts;
                }
                $ppg = (int)get_option('posts_per_page');
                $ii = 0;
                for ($i = ($pg == 1 ? 0 : (($pg - 1) * $ppg)); $i < count($posts); $i++) { ?>
                    <div class="row">
                        <div class="col-md-1">
                            <input type="checkbox" class="wd_notify_id" id="notifylist_<?= $posts[$i] ?>" value="<?= $posts[$i] ?>">
                        </div>
                        <div class="col-md-2">
                            <time><?= get_the_date(get_option('date_format'), $posts[$i]) ?></time>
                        </div>
                        <div class="col-md-7">
                            <?= apply_filters('the_content', get_post_field('post_content', $posts[$i])) ?>
                        </div>
                        <div class="col-md-1">
                            <img src="<?= esc_url(get_template_directory_uri()) ?>/img/icon-delete.svg" data-notifyid="<?= $posts[$i] ?>" data-unread="<?= $unread ?>" class="dc_notification_delete" title="<?= __('Delete notification', 'base') ?>" alt="delete">
                        </div>
                        <div class="col-md-1">
                            <img class="dc_notyfication_openclose" id="wd_notify_<?= $posts[$i] ?>" src="<?= ((int)get_post_meta($posts[$i], 'readed', true) ?notifyRead :notifyURead) ?>" data-notyid="<?= $posts[$i] ?>" title="<?= __('Read notification', 'base') ?>" alt="read">
                        </div>
                    </div><?php
                    if(++$ii >= $ppg){
                        break;
                    }
                } ?>
                <?= sitePagingAJAX(($unread == 1 ? 'paging_notifys_rd' : 'paging_notifys_ur'), count($posts), $pg, $ppg) ?>
            </div>

        </div>
    </section><?php
}


//===DELETE NOTIFY===
if (isset($_GET['delnotid']) && $_GET['delnotid'] > 0){
    $_GET['delnotid'] = (int)$_GET['delnotid'];
    $post = get_post($_GET['delnotid']);
    if ($post) {
        if ($post->post_author == get_current_user_id()){
            wp_delete_post($_GET['delnotid'], true);
        }
    }
}


/* Register a custom post type called "System log". */
function dc_f7_init() {
    $labels = array(
        'name'                  => _x( 'Notify posts', 'Post type general name', 'base' ),
        'singular_name'         => _x( 'Notify posts', 'Post type singular name', 'base' ),
        'menu_name'             => _x( 'Notify posts', 'Admin Menu text', 'base' ),
        'name_admin_bar'        => _x( 'Notify posts', 'Add New on Toolbar', 'base' ),
        'add_new'               => __( 'Add New', 'base' ),
        'add_new_item'          => __( 'Add New', 'base' ),
        'new_item'              => __( 'New Notify posts', 'base' ),
        'edit_item'             => __( 'Edit Notify posts', 'base' ),
        'view_item'             => __( 'View Notify posts', 'base' ),
        'all_items'             => __( 'All Notify posts', 'base' ),
        'search_items'          => __( 'Search Notify posts', 'base' ),
        'parent_item_colon'     => __( 'Parent Notify posts:', 'base' ),
        'not_found'             => __( 'No Notify posts found.', 'base' ),
        'not_found_in_trash'    => __( 'No Notify posts found in Trash.', 'base' ),
        'featured_image'        => _x( 'Cover image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'base' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'base' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'base' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'base' ),
        'archives'              => _x( 'Notify posts archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'base' ),
        'insert_into_item'      => _x( 'Insert into Notify posts', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'base' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Notify posts', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'base' ),
        'filter_items_list'     => _x( 'Filter Notify posts list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'base' ),
        'items_list_navigation' => _x( 'Notify posts list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'base' ),
        'items_list'            => _x( 'Notify posts list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'base' ),
    );
    
    $args = array(
        'labels'             => $labels,
        'description'        => 'Notify custom post type.',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => false,
        'query_var'          => true,
        'rewrite'            => array( 'slug'=>'syslog' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 20,
        'supports'           => array( 'title', 'editor' ), //, 'excerpt', 'comments', 'author', 'thumbnail'
        'taxonomies'         => array(),
        'show_in_rest'       => true
    );
    register_post_type( 'syslog', $args );
    
}
add_action( 'init', 'dc_f7_init' );


function getNotifyReaded(){
    return get_posts(['post_type' => 'syslog',
            'fields'      => 'ids',
            'author'      => get_current_user_id(),
            'meta_key'    => 'readed',
            'meta_value'  => '1',
            'numberposts' => 51,]
    );
}


function getNotifyUnReaded(){
    return get_posts(['post_type' => 'syslog',
            'fields'      => 'ids',
            'author'      => get_current_user_id(),
            'meta_key'    => 'readed',
            'meta_value'  => '0',
            'numberposts' => 51,]
    );
}


//===system log===
function setLog($toUserID = 0, $message = ''){
    $toUserID = (int)$toUserID;
    $logID = wp_insert_post(array(
        'post_title'   => 'to-' . $toUserID,
        'post_content' => sanitize_text_field($message),
        'post_status'  => 'publish',
        'post_type'    => 'syslog',
        'post_author'  => $toUserID,
    ));
    update_post_meta($logID, 'readed', 0);
}


//===system log email===
function setLogEmail($toUserID = 0, $message = ''){
    $toUserID = (int)$toUserID;
    $user = get_user_by('id', $toUserID);
    if ($user) {
        siteMail($user->user_email, $message);
    }
}


function getNotifyUnReadedCount(){
    return count(getNotifyUnReaded());
}


//===read notify arr===
add_action('wp_ajax_readnotifyarr',        'readnotifyarr');
//add_action('wp_ajax_nopriv_readnotifyarr', 'readnotifyarr');
function readnotifyarr(){
	//$res             = '';
	$_POST['msgarr'] = $_POST['msgarr'] ?? '[]';
	$_POST['msgarr'] = (array)$_POST['msgarr'];
	$_POST['readed'] = $_POST['readed'] ?? 0;
	$_POST['readed'] = (int)$_POST['readed'];

	foreach($_POST['msgarr'] as $msgid){
		$post = get_post((int)$msgid);
		if($post){
			if($post->post_author == get_current_user_id()){
				update_post_meta($post->ID, 'readed', 1);
				//$res = __('Notify deleted!', 'base');
			}else{
				//$res = __('You are no author this notify!', 'base');
			}
		}else{
			//$res = __('Notify not exists!', 'base');
		}
	}
	
	echo page_notifys(0, $_POST['readed']);
	die();
}

//===unread notify arr===
add_action('wp_ajax_unreadnotifyarr',        'unreadnotifyarr');
//add_action('wp_ajax_nopriv_unreadnotifyarr', 'unreadnotifyarr');
function unreadnotifyarr(){
	//$res             = '';
	$_POST['msgarr'] = $_POST['msgarr'] ?? '[]';
	$_POST['msgarr'] = (array)$_POST['msgarr'];
	$_POST['readed'] = $_POST['readed'] ?? 0;
	$_POST['readed'] = (int)$_POST['readed'];

	foreach($_POST['msgarr'] as $msgid){
		$post = get_post((int)$msgid);
		if($post){
			if($post->post_author == get_current_user_id()){
				update_post_meta($post->ID, 'readed', 0);
				//$res = __('Notify deleted!', 'base');
			}else{
				//$res = __('You are no author this notify!', 'base');
			}
		}else{
			//$res = __('Notify not exists!', 'base');
		}
	}
	
	echo page_notifys(0, $_POST['readed']);
	die();
}

//===delete notify arr===
add_action('wp_ajax_deletenotifyarr',        'deletenotifyarr');
//add_action('wp_ajax_nopriv_deletenotifyarr', 'deletenotifyarr');
function deletenotifyarr(){
	//$res             = '';
	$_POST['msgarr'] = $_POST['msgarr'] ?? '[]';
	$_POST['msgarr'] = (array)$_POST['msgarr'];
	$_POST['readed'] = $_POST['readed'] ?? 0;
	$_POST['readed'] = (int)$_POST['readed'];

	foreach($_POST['msgarr'] as $msgid){
		$post = get_post((int)$msgid);
		if($post){
			if($post->post_author == get_current_user_id()){
				wp_delete_post($post->ID);
				//$res = __('Notify deleted!', 'base');
			}else{
				//$res = __('You are no author this notify!', 'base');
			}
		}else{
			//$res = __('Notify not exists!', 'base');
		}
	}
	
	echo page_notifys(0, $_POST['readed']);
	die();
}

//===DELETE notyce===
add_action('wp_ajax_delnotify',        'delnotify');
//add_action('wp_ajax_nopriv_delnotify', 'delnotify');
function delnotify(){
	$notifyid = $_POST['notifyid'] ?? 0;
	$notifyid = (int)$notifyid;
	
	$unread = $_POST['unread'] ?? 0;
	$unread = (int)$unread;

	$post = get_post($notifyid);
	if( $post ){
		if( get_current_user_id() == $post->post_author ){
			wp_delete_post( $notifyid, true );
			page_notifys(1, $unread);
		}else{
			_e('You cannot delete this notyce!', 'base');
		}
	}else{
		_e('The notyce does not exist!', 'base');
	}
	
	die();
}

//===OPEN/CLOSE notyfy===
add_action('wp_ajax_notyopenclose',        'notyopenclose');
//add_action('wp_ajax_nopriv_notyopenclose', 'notyopenclose');
function notyopenclose(){
	$_POST['notyid'] = (int)$_POST['notyid'];
	
	$post = get_post($_POST['notyid']);
	if($post){
		if($post->post_author == get_current_user_id()){
			if( (int)get_post_meta($_POST['notyid'], 'readed', true) ){
				update_post_meta($_POST['notyid'], 'readed', 0);
				echo 0;
			}else{
				update_post_meta($_POST['notyid'], 'readed', 1);
				echo 1;
			}
		}
	}
	die();
} ?>