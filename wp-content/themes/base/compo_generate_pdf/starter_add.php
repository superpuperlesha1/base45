<script>
    document.addEventListener('DOMContentLoaded', function(){

        //===MENU EDIT MY ACCOUNT===
        $(document).on('click', '.w51p_menu_pdf', function(event){
            event.stopPropagation();
            event.preventDefault();
            setURLpage($(this).find('a').attr('href'));
            //var userid = $(this).find('a').attr('data-userid');
            $('#base_main').fadeTo(fadeSpeed, 0, 'linear', function(){  });
            $.ajax({
                type: 'POST',
                url:  WPajaxURL,
                data: {
                        action: 'pagepdf',
                      },
                success:     function(data, textStatus, XMLHttpRequest) {
                    $('#base_main').html(data);
                    $('#base_main').fadeTo('slow', 1, function(){  });
                },
                error: function(MLHttpRequest, textStatus, errorThrown) {
                    top.location.href = WPURL;
                }
            });
        });

    });
</script>