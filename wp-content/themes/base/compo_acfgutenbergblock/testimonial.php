<?php

$id    = 'testimonial-' . $block['id'];
$text  = get_field('testimonial') ?: 'Your testimonial here...';
$image = get_field('image') ?: 295; ?>

<div id="<?= esc_attr($id) ?>">
	<p><?= $text ?></p>
	<?php echo wp_get_attachment_image( $image, 'thumbnail' ); ?>
</div>