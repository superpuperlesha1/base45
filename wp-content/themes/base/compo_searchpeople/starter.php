<?php

define('hartFilled',     esc_url(get_template_directory_uri()).'/img/hart_y.svg');
define('hartNFilled',    esc_url(get_template_directory_uri()).'/img/hart.svg');
define('km_mile',        1.609344);

/*===MENU PAGE SEARCH===
$id = get_field('sopt_search_pid', 'option') ?>
<li class="w51p_menu_search">
	<a href="<?= esc_url(get_the_permalink($id)) ?>" data-pageid="<?= $id ?>"><?= get_the_title($id) ?></a>
</li>*/
//===PAGE SEARCH===
add_action('wp_ajax_menusearch',        'menusearch');
//add_action('wp_ajax_nopriv_menusearch', 'menusearch');
function menusearch(){
	$_POST['pg']                  = $_POST['pg']                  ?? '';
	$_POST['event_search_radius'] = $_POST['event_search_radius'] ?? '';
	$_POST['event_search_txt']    = $_POST['event_search_txt']    ?? '';
	$_POST['event_search_order']  = $_POST['event_search_order']  ?? '';
	page_search($_POST['pg'], $_POST['event_search_radius'], $_POST['event_search_txt'], $_POST['event_search_order']);
	die();
}


add_action( 'wp_footer', 'action_function_name_2479' );
function action_function_name_2479(){
	get_template_part('/compo_searchpeople/starter_add');
}


//===PAGE SEARCH===
function page_search($pg=1, $event_search_radius=5, $event_search_txt='', $event_search_order='registered'){

    //===CHECK PAYMENT===
    $checkUserCS = checkUserCS(true);
    if($checkUserCS){
        echo $checkUserCS;
        exit();
    }

	//===VALIDATION===
	$pg = (int)$pg;
	if($pg<1){
		$pg = 1;
	}

	$event_search_radius = (int)$event_search_radius;
	if($event_search_radius<5 || $event_search_radius>50){
		$event_search_radius = 5;
	}

	$event_search_txt = sanitize_text_field($event_search_txt);
	if( strlen($event_search_txt)>30){
		$event_search_txt = substr($event_search_txt, 0, 30);
	}

	$event_search_order = sanitize_text_field($event_search_order);
	if( !in_array($event_search_order, ['registered', 'first_name', 'visiting']) ){
		$event_search_order = 'registered';
	}

    $usr_eb     = get_user_meta(get_current_user_id(), 'usr_eb', true);
    $usr_zip_lt = (float)get_user_meta(get_current_user_id(), 'usr_zip_lt', true);
    $usr_zip_ln = (float)get_user_meta(get_current_user_id(), 'usr_zip_ln', true); ?>

    <section class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?= get_the_title(get_field('sopt_search_pid', 'option')) ?></h1>
                <?= apply_filters('the_content', get_post_field('post_content', get_field('sopt_search_pid', 'option'))) ?>
                <form action="#uID" method="POST" class="row">
                    <div class="form-group col-md-3">
                        <label for="event_search_radius"><?= __('Distance', 'base') ?></label>
                        <input type="number" id="event_search_radius" name="event_search_radius" value="<?= $event_search_radius ?>" min="5" max="50" step="1" oninput="this.value=this.value.replace(/[^0-9.]/g, ''); " class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                    	<label for="event_search_txt"><?= __('Name, origin, residence.', 'base') ?></label>
                        <input type="search" id="event_search_txt" name="event_search_txt" value="<?= $event_search_txt ?>" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                    	<label for="event_search_order"><?= __('Sort by:', 'base') ?></label>
	                    <select id="event_search_order" name="event_search_order" class="form-control" data-toggle="tooltip" data-placement="top" title="<?= __('Sort search results', 'base') ?>">
                            <option value="registered" <?= $event_search_order=='registered' ?'selected' :'' ?>><?= __('Newest',       'base') ?></option>
                            <option value="first_name" <?= $event_search_order=='first_name' ?'selected' :'' ?>><?= __('Alphabetical', 'base') ?></option>
                            <option value="visiting"   <?= $event_search_order=='visiting'   ?'selected' :'' ?>><?= __('Visiting',     'base') ?></option>
                        </select>
                	</div>
                    <div class="form-group col-md-3">
                    	<label></label>
                        <input type="hidden" id="event_search_pg" name="event_search_pg" value="<?= $pg ?>">
	                    <button type="submit" class="btn btn-primary event_search_submitter form-control"><?= __('Search', 'base') ?></button>
                    </div>
                </form>
            </div>
            <div class="col-md-7"><?php
                $userArr = getUserArrByRadius($usr_zip_lt, $usr_zip_ln, $event_search_radius, $usr_eb, current_user_can('administrator'), get_current_user_id());

				//===SEARCH INPUT===
				if($event_search_txt){
					$arr = [];
					foreach( $userArr as $userItem ){
						$p1 = get_user_meta($userItem['ID'], 'usr_info_1', true);
						$p2 = get_user_meta($userItem['ID'], 'usr_info_2', true);
						$p3 = get_user_meta($userItem['ID'], 'usr_info_3', true);
						if( stripos($userItem['fname'], $event_search_txt) !== false ||
							stripos($p1,                $event_search_txt) !== false ||
							stripos($p2,                $event_search_txt) !== false ||
							stripos($p3,                $event_search_txt) !== false ){
							$arr[] = $userItem;
						}
					}
					$userArr = $arr;
				}

				//===ORDER===
				if($event_search_order == 'registered'){
					foreach ($userArr as $key => $row) {
					    $volume[$key]  = $row['ts'];
					}
					$volume = array_column($userArr, 'ts');
					array_multisort($volume, SORT_DESC, $userArr);
				}
				if($event_search_order == 'first_name'){
					foreach ($userArr as $key => $row) {
					    $volume[$key]  = $row['fname'];
					}
					$volume = array_column($userArr, 'fname');
					array_multisort($volume, SORT_ASC, $userArr);
				}
				if($event_search_order == 'visiting'){
					foreach ($userArr as $key => $row) {
					    $volume[$key]  = $row['visit'];
					}
					$volume = array_column($userArr, 'visit');
					array_multisort($volume, SORT_DESC, $userArr);
				}
                ?>
                
                <h4><?= count($userArr) ?> <?= __('people found', 'base') ?></h4>
                <div><?php
					$mapARR = [];
					$ppg = (int)get_option('posts_per_page');
					$ii  = 0;
					for($i=($pg==1 ?0 :(($pg-1)*$ppg));$i<count($userArr);$i++){
						get_template_part('compo_searchpeople/item_user_full', '', $userArr[$i]);
						$mapARR[] = $userArr[$i];
						if(++$ii>=$ppg){break;}
					}
					echo sitePagingAJAX('paging_search', count($userArr), $pg, $ppg); ?>
                </div>
            </div>
            <div class="col-md-5">
                <div id="dc_search_map_box" style="min-height:550px;"></div>
				<script>
					var dc_search_map_arr    = <?= json_encode($mapARR) ?>;
					var dc_search_map_my_lt  = <?= $usr_zip_lt ?>;
					var dc_search_map_my_ln  = <?= $usr_zip_ln ?>;
					var dc_search_map_radius = <?= $event_search_radius * 1000 * ($usr_eb=='Mi' ?km_mile :1) ?>;
				</script>
                <h4><?= __('Favorites', 'base') ?></h4>
                <div id="dc_favlist_box" class="search__remembered-list">
                    <?= getMyFavoritList() ?>
                </div>
            </div>
        </div>
    </section><?php
}


//===my favorites LIST===
function getMyFavoritList(){
    $res = '';
    $arr = (array)unserialize(get_user_meta(get_current_user_id(), 'my_favorites', true));
    foreach ($arr as $arri) {
        $xuser = get_user_by('id', $arri);
        if ($xuser) {
            $res .= '<div class="search__remembered-human">
				        <a href="' . get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) . '/?profile=' . $arri . '" class="dc_myacc_view" data-userid="' . $arri . '">
				        	<img src="' . get_myavatar_url($arri, 'thumbnail_64x64') . '" alt="Ava">
				        </a>
				        <div class="search__remembered-info">
							<strong>
								<a href="' . get_page_uri(get_field('sopt_myprofile_view_pid', 'option')) . '/?profile=' . $arri . '" class="dc_myacc_view" data-userid="' . $arri . '">' . $xuser->first_name . '</a>
							</strong>
							<time>' . __('On the website since:', 'base') . ' ' . date(get_option('date_format'), strtotime($xuser->user_registered)) . '</time>
							<button>
				        	    <img src="' . (get_yn_user_fav($arri) ? hartFilled : hartNFilled) . '" id="usr_fav_' . $arri . '" class="dc_favicon_del" data-favid="' . $arri . '" alt="fav">
				            </button>
				        </div>
					</div>';
        } else {
            $farr = (array)unserialize(get_user_meta(get_current_user_id(), 'my_favorites', true));
            unset($farr[array_search($arri, $farr)]);
            $farr = array_values($farr);
            update_user_meta(get_current_user_id(), 'my_favorites', serialize($farr));
        }
    }
    return $res;
}


//===GETTING COUNT USER TO MY ARREA===
function getCountUserArea($userID=0){
    $userID     = (int)$userID;
    $usr_zip_lt = (float)get_user_meta($userID, 'usr_zip_lt', true);
    $usr_zip_ln = (float)get_user_meta($userID, 'usr_zip_ln', true);
    $userArr    = getUserArrByRadius($usr_zip_lt, $usr_zip_ln, 20);
    return count($userArr);
}


function getUserArrByRadius($my_lt, $my_ln, $radius, $usr_eb='Km', $ap=false, $uIDignor=0){
    GLOBAL $wpdb;
    $res     = [];
    $usr_ebq = $usr_eb;
    $ap      = (int)$ap;
    $usr_eb  = ($usr_eb == 'Km' ?1 :1.609344);
    $ts      = time();

    $sql = "SELECT  sd.fname             as fname,
				    sd.reg               as reg,
				    sd.ID                as ID,
				    sd.usr_ziptrip_day_s as usr_ziptrip_day_s,
				    sd.usr_ziptrip_day_f as usr_ziptrip_day_f,
				    sd.trip              as trip,
				    sd.admin_blocked     as admin_blocked,
				    sd.usr_visible       as usr_visible,
				    sd.admin_payment     as admin_payment,
				    sd.role              as role,
				    sd.login_count       as login_count,
				    IF(sd.trip=0, sd.distance,    sd.tdistance)       as distance,
					IF(sd.trip=0, sd.usr_zip_lt,  sd.usr_ziptrip_lt)  as usr_zip_lt,
					IF(sd.trip=0, sd.usr_zip_ln,  sd.usr_ziptrip_ln)  as usr_zip_ln,
					IF(sd.trip=0, sd.usr_zip_loc, sd.usr_ziptrip_loc) as usr_zip_loc
				FROM (
				SELECT  u.ID as ID,
						um0.meta_value    as fname,
				        um1.meta_value    as usr_zip_lt,
					    um2.meta_value    as usr_zip_ln,
					    um3.meta_value    as usr_visible,
					    um5.meta_value    as usr_ziptrip_day_s,
					    um10.meta_value   as usr_ziptrip_day_f,
					    um6.meta_value    as usr_ziptrip_lt,
					    um8.meta_value    as usr_zip_loc,
					    um9.meta_value    as usr_ziptrip_loc,
					    um7.meta_value    as usr_ziptrip_ln,
					    um12.meta_value   as role,
					    u.user_registered as reg,
					    um4.meta_value    as admin_blocked,
					    um11.meta_value   as admin_payment,
					    um13.meta_value   as login_count,
					    IF(um5.meta_value <= " . $ts . " && um10.meta_value >= " . $ts . ", 1, 0) as trip,
						(
				            (
				                (
				                    acos(
				                        sin(( $my_lt * pi() / 180))
				                        *
				                        sin(( um1.meta_value * pi() / 180)) + cos(( $my_lt * pi() /180 ))
				                        *
				                        cos(( um1.meta_value * pi() / 180)) * cos((( $my_ln - um2.meta_value) * pi()/180)))
				                ) * 180/pi()
				            ) * 60 * 1.1515 * $usr_eb
				        ) as distance,
				        (
				            (
				                (
				                    acos(
				                        sin(( $my_lt * pi() / 180))
				                        *
				                        sin(( um6.meta_value * pi() / 180)) + cos(( $my_lt * pi() /180 ))
				                        *
				                        cos(( um6.meta_value * pi() / 180)) * cos((( $my_ln - um7.meta_value) * pi()/180)))
				                ) * 180/pi()
				            ) * 60 * 1.1515 * $usr_eb
				        ) as tdistance
			    
			    FROM $wpdb->users u
			    
			    LEFT JOIN $wpdb->usermeta um0  ON u.ID=um0.user_id   && um0.meta_key   = 'first_name'
			    LEFT JOIN $wpdb->usermeta um1  ON u.ID=um1.user_id   && um1.meta_key   = 'usr_zip_lt'
			    LEFT JOIN $wpdb->usermeta um2  ON u.ID=um2.user_id   && um2.meta_key   = 'usr_zip_ln'
			    LEFT JOIN $wpdb->usermeta um3  ON u.ID=um3.user_id   && um3.meta_key   = 'usr_visible'
			    LEFT JOIN $wpdb->usermeta um4  ON u.ID=um4.user_id   && um4.meta_key   = 'admin_blocked'
			    LEFT JOIN $wpdb->usermeta um5  ON u.ID=um5.user_id   && um5.meta_key   = 'usr_ziptrip_day_s'
			    LEFT JOIN $wpdb->usermeta um10 ON u.ID=um10.user_id  && um10.meta_key  = 'usr_ziptrip_day_f'
			    LEFT JOIN $wpdb->usermeta um6  ON u.ID=um6.user_id   && um6.meta_key   = 'usr_ziptrip_lt'
			    LEFT JOIN $wpdb->usermeta um7  ON u.ID=um7.user_id   && um7.meta_key   = 'usr_ziptrip_ln'
			    LEFT JOIN $wpdb->usermeta um8  ON u.ID=um8.user_id   && um8.meta_key   = 'usr_zip_loc'
			    LEFT JOIN $wpdb->usermeta um9  ON u.ID=um9.user_id   && um9.meta_key   = 'usr_ziptrip_loc'
			    LEFT JOIN $wpdb->usermeta um11 ON u.ID=um11.user_id  && um11.meta_key  = 'admin_payment'
			    LEFT JOIN $wpdb->usermeta um12 ON u.ID=um12.user_id  && um12.meta_key  = 'tw51p_capabilities'
			    LEFT JOIN $wpdb->usermeta um13 ON u.ID=um13.user_id  && um13.meta_key  = 'login_count'
			    
			    WHERE u.ID != " . (int)$uIDignor . "
			) sd
			WHERE ( distance  <= $radius AND trip = 0 )
				    ||
				  ( tdistance <= $radius AND trip = 1 )";
    $users = $wpdb->get_results($sql);
    if ($users) {
        foreach ($users as $suser) {
            if (($ap == 1 || ($suser->admin_payment == 1 && $suser->admin_blocked == 0 && $suser->usr_visible == 1) && strpos($suser->role, s51m_user_role) !== false)) {
                $lt = (float)$suser->usr_zip_lt;
                $ln = (float)$suser->usr_zip_ln;
                $ttss = (int)$suser->usr_ziptrip_day_s;
                $ttsf = (int)$suser->usr_ziptrip_day_f;
                $dc = (float)$suser->distance;
                $zip = htmlspecialchars($suser->usr_zip_loc);
                $title = $suser->fname . chr(10) . $zip . chr(10) . round($dc, 2) . $usr_ebq . '.'; //'(' . $lt . ',' . $ln . ')'
                $visit = $suser->login_count;
                $res[] = ['ttss' => $ttss, 'ttsf' => $ttsf, 'admin_payment' => (int)$suser->admin_payment, 'usr_visible' => (int)$suser->usr_visible, 'admin_blocked' => (int)$suser->admin_blocked, 'ID' => (int)$suser->ID, 'trip' => (int)$suser->trip, 'loc' => $zip, 'visit' => (int)$visit, 'fname' => $suser->fname, 'ts' => strtotime($suser->reg), 'lt' => $lt, 'ln' => $ln, 'dc' => $dc, 'title' => $title];
            }
        }
    }
    return $res;
}


function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Mi'){
    $theta = $longitude1 - $longitude2;
    $distance = sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta));

    $distance = acos($distance);
    $distance = rad2deg($distance);
    $distance = $distance * 60 * 1.1515;

    switch ($unit) {
        case 'Mi':
            break;
        case 'Km':
            $distance = $distance * 1.609344;
    }

    return (round($distance, 2));
}



//===dell favorite from list===
add_action('wp_ajax_favdel',        'favdel');
//add_action('wp_ajax_nopriv_favdel', 'favdel');
function favdel(){
	$favid = $_POST['favid'] ?? (int)$_POST['favid'] ?? 0;
	$arr = (array)unserialize(get_user_meta(get_current_user_id(), 'my_favorites', true));
	if(array_search($favid, $arr) !== false){
		unset($arr[array_search($favid, $arr)]);
		$arr = array_values($arr);
	}else{
		$arr[] = $favid;
	}

	update_user_meta(get_current_user_id(), 'my_favorites', serialize($arr));
	echo getMyFavoritList();
	die();
}


//===add/dell favorite in search===
add_action('wp_ajax_favadddel',        'favadddel');
//add_action('wp_ajax_nopriv_favadddel', 'favadddel');
function favadddel(){
	$res = ['itype'=>0, 'txt'=>''];
	$favid = $_POST['favid'] ?? (int)$_POST['favid'] ?? 0;
	$arr = (array)unserialize(get_user_meta(get_current_user_id(), 'my_favorites', true));
	if(array_search($favid, $arr) !== false){
		unset($arr[array_search($favid, $arr)]);
		$arr = array_values($arr);
		$res['itype'] = 0;
	}else{
		$arr[] = $favid;
		$res['itype'] = 1;
	}

	update_user_meta(get_current_user_id(), 'my_favorites', serialize($arr));
	$res['txt'] = getMyFavoritList();
	echo json_encode($res);
	die();
}


//===Y/N user in favorites===
function get_yn_user_fav($userID){
    if (array_search((int)$userID, (array)unserialize(get_user_meta(get_current_user_id(), 'my_favorites', true))) === false) {
        return false;
    } else {
        return true;
    }
} ?>