<?php
/*
Template Name: Page [CSV]
*/
    
    
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header( 'Content-Disposition: attachment;filename='.microtime(true).'.csv');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    echo "\xEF\xBB\xBF"; // UTF-8 BOM
    
    
	$fp = fopen('php://output', 'x');
	$query1 = new WP_Query([
			'posts_per_page' => 999999,
			'offset'         => 0,
			'post_type'      => 'sdods',
			'post_status'    => 'publish',
			'meta_query'     => [
				[
					'key'    => 'nd_dog_status',
					'value'  => 1,
				],
			],
	]);
	while($query1->have_posts()){
		$query1->the_post();
		$fields = [
			get_the_date('d.m.Y'),
			get_post_meta(get_the_id(), 'nd_program',       true),
			get_post_meta(get_the_id(), 'nd_dog_sn',        true)
		];
		fputcsv($fp, $fields);
	}
	
	fclose($fp);
	
?>