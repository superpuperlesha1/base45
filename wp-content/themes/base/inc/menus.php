<?php


//===Theme main front menus===
register_nav_menus( array(
	'HeadMenu' => __( 'Header menu', 'base' ),
	'FootSoc'  => __( 'Social menu', 'base' ),
	'FootMenu' => __( 'Footer menu', 'base' ),
) );


//===ACF options admin menu===
if( function_exists( 'acf_add_options_sub_page' ) ) {
	acf_add_options_page( array(
		'title'  => __('Site options', 'base'),
		'parent' => 'themes.php',
	) );
}


add_action('admin_menu', 'register_my_custom_submenu_page');


function register_my_custom_submenu_page(){
	//===main admin menu===
	//add_submenu_page('themes.php', 'Testimonials',   'Testimonials',  'administrator', 'edit.php?post_type=testimonial');
	//add_submenu_page('themes.php', 'FAQ\'s',         'FAQ\'s',        'administrator', 'edit.php?post_type=faq');
	//add_submenu_page('themes.php', 'Events',         'Events',        'administrator', 'edit.php?post_type=event');
	//add_submenu_page('themes.php', 'gmsg',           'gmsg',        'administrator', 'edit.php?post_type=gmsg');
	//add_submenu_page('themes.php', 'pmsg',           'pmsg',        'administrator', 'edit.php?post_type=pmsg');
	//add_submenu_page('themes.php', 'Test quest 1',   'Test quest 1',  'administrator', 'edit.php?post_type=testq1');
	//add_submenu_page('themes.php', 'Test quest 2',   'Test quest 2',  'administrator', 'edit.php?post_type=testq2');
}


//===SOCIAL SHARE MENU===
function nmssmshare_list($ulClass='', $pageTitle='', $pageURL='', $imgURL){
	//https://www.business.com/articles/create-share-buttons/
	$res='<ul class="'.$ulClass.'">';
	if(have_rows('socialsh_list', 'option')){
		while(have_rows('socialsh_list', 'option')){
			the_row();
			$pageTitle = urlencode($pageTitle);
			$pageURL   = urlencode($pageURL);
			$imgURL    = urlencode($imgURL);
			$snt = get_sub_field('socialsh_list_url', 'option');
			if($snt=='facebook'){
				$res .='<li>
							<a  href    = "#uID" 
								target  = "_blank" 
								rel     = "nofollow" 
								onClick = "window.open(\'http://www.facebook.com/sharer.php?s=100&amp;p[title]='.$pageTitle.'&amp;p[url]='.$pageURL.'&amp;&p[images][0]='.$imgURL.'\', \'sharer\', \'toolbar=0,status=0,width=548,height=325\');" target="_parent" href="javascript: void(0)">'.get_sub_field('socialsh_list_svg', 'option').'
							</a>
						</ll>';
			}elseif($snt=='twitter'){
				$res .='<li>
							<a  href    = "#uID" 
								target  = "_blank" 
								rel     = "nofollow" 
								onClick = "window.open(\'http://twitter.com/home?status=Currentlyreading '.$pageURL.'\', \'sharer\', \'toolbar=0,status=0,width=548,height=325\');" target="_parent" href="javascript: void(0)">'.get_sub_field('socialsh_list_svg', 'option').'
							</a>
						</ll>';
			}elseif($snt=='linkedin'){
				$res .='<li>
							<a  href    = "#uID" 
								target  = "_blank" 
								rel     = "nofollow" 
								onClick = "window.open(\'http://www.linkedin.com/shareArticle?mini=true&url='.$pageURL.'&title=$pageTitle&source='.$pageURL.'\', \'sharer\', \'toolbar=0,status=0,width=548,height=325\');" target="_parent" href="javascript: void(0)">'.get_sub_field('socialsh_list_svg', 'option').'
							</a>
						</ll>';
			}else{
				
			}
		}
	}
	$res.='</ul>';
	return $res;
}