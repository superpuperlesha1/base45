<?php
// Theme thumbnails
add_theme_support('post-thumbnails');

add_image_size('thumbnail_1920x1080', 1920, 1080, true);
add_image_size('thumbnail_300x200',   300,  200,  true);
add_image_size('thumbnail_100x100',   100,  100,  true);


