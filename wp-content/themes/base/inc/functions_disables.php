<?php
	
	//===HIDE ADMIN BAR for simple users===
	if(!current_user_can('manage_options')){
		add_filter('show_admin_bar', '__return_false');
	}

	
	//===Remove tags from posts===
	function myprefix_unregister_tags(){
		unregister_taxonomy_for_object_type('post_tag', 'post');
	}
	add_action('init', 'myprefix_unregister_tags');
	
	
	//===Disable embeds on init===
	add_action('init', function(){
		remove_action('wp_head',             'wp_oembed_add_discovery_links');
		remove_action('wp_head',             'wp_oembed_add_host_js');
		remove_action('wp_head',             'wp_generator');
		remove_action('wp_head',             'print_emoji_detection_script', 7);
		remove_action('admin_print_scripts', 'print_emoji_detection_script');
		remove_action('wp_print_styles',     'print_emoji_styles');
		remove_action('admin_print_styles',  'print_emoji_styles');
		remove_filter('the_content_feed',    'wp_staticize_emoji');
		remove_filter('comment_text_rss',    'wp_staticize_emoji');
		remove_filter('wp_mail',             'wp_staticize_emoji_for_email');
	}, PHP_INT_MAX - 1);
	
	
	//===disable GUTTENBERG for posts===
	//add_filter('use_block_editor_for_post', '__return_false', 10);
	//===disable GUTTENBERG for post types===
	//add_filter('use_block_editor_for_post_type', '__return_false', 10);
	
	
	//===disable color sheme==
	remove_action('admin_color_scheme_picker', 'admin_color_scheme_picker');
	//===disable QTRANSLATE GENERATOR PLUGIN NAME===
	//remove_action('wp_head','qtranxf_wp_head_meta_generator');
	
	
	/*//===remove defoult editor from pages===
	add_action('admin_init', 'hide_editor');
	function hide_editor(){
		if(isset($_POST['post_ID'])){$post_id = $_POST['post_ID']; }
		if(isset($_GET['post'])    ){$post_id = $_GET['post'];     }
		if(!isset($post_id))return;
		$tpn = get_page_template_slug($post_id);
		if(in_array($tpn, ['template_page/page_flexible_1.php'])){
			remove_post_type_support('page', 'editor');
		}
	}*/
		
		
	//===hide file editors===
	function disable_mytheme_action() {
		define('DISALLOW_FILE_EDIT', TRUE);
		//define('DISALLOW_FILE_MODS', TRUE);
    }
    add_action('init','disable_mytheme_action');
	
?>