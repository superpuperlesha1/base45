<?php
/**
 ** Template Main List TimeTable
 **/

$post_id;              /* Post ID */
$ts_start;              /* start timestamp */
$dscc_timearrea      = \Rent_Angry_Cat_ns\Rent_Angry_Cat::get_dscc_timearrea( $post_id ); /* post rated hours */
$dscc_datearrea      = \Rent_Angry_Cat_ns\Rent_Angry_Cat::get_dscc_datearrea( $post_id ); /* post rated days */
$dscc_time_from_rate = \Rent_Angry_Cat_ns\Rent_Angry_Cat::getPostTimeFromRate( $post_id ); /* rate start time */
$dscc_time_to_rate   = \Rent_Angry_Cat_ns\Rent_Angry_Cat::getPostTimeToRate( $post_id ); /* rate finish time */
$table_style         = \Rent_Angry_Cat_ns\Rent_Angry_Cat::getPostTTableStyle( $post_id ); /* rate table style */
$today_ts            = strtotime( 'today' ); /* timestamp today */

$parentID = 'dscc_daybox_id_' . $post_id;
$img      = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'thumbnail_250x200' );

$res_template .= '<a href             = "#uID"
					 class            = "dscc_itemcartable_prev"
					 data-carid       = "' . esc_attr( $post_id ) . '"
					 data-tsstart     = "' . esc_attr( $ts_start ) . '"
					 data-id          = "' . esc_attr( 'car_' . $post_id ) . '"
					 data-deltadays   = "7"
				  >
					&LT;' . esc_html( __( 'Prev', 'rent_angry_cat' ) ) . '
				  </a>
				  <a href             = "#uID"
					 class            = "dscc_itemcartable_next"
					 data-carid       = "' . esc_attr( $post_id ) . '" 
					 data-tsstart     = "' . esc_attr( $ts_start ) . '"
					 data-id          = "' . esc_attr( 'car_' . $post_id ) . '"
					 data-deltadays   = "7"
				  >
					' . esc_html( __( 'Next', 'rent_angry_cat' ) ) . '&GT;
				  </a>
	              <a href             = "#uID"
				     class            = "dscc_itemcartable_order" 
	                 data-styleid     = "' . esc_attr( $table_style ) . '"
	                 data-carid       = "' . esc_attr( $post_id ) . '"
	                 data-parentboxid = "' . esc_attr( $parentID ) . '"
	                 data-error_date  = "' . esc_attr( __( 'Select Date/Time!', 'rent_angry_cat' ) ) . '"
	              >
					' . esc_html( __( 'Order', 'rent_angry_cat' ) ) . '
				  </a>
	              <table  id="' . $parentID . '" border="1">
	                <tr>';

for ( $i = 0; $i < 9; $i ++ ) {
	$setday   = strtotime( ' +' . $i . ' day', $ts_start );
	$freeDay  = ! in_array( $setday, $dscc_datearrea );
	$featured = ( $setday >= $today_ts ? true : false );

	$res_template .= '<td class="' . ( $freeDay && $featured ? 'dscc_bg_yes' : 'dscc_bg_none' ) . '">
                                          <b>' . date( 'd.m', strtotime( ' +' . $i . ' day', $ts_start ) ) . '</b>';

	if ( $table_style == 'tablefull' ) {
		$disable   = ( $freeDay && $featured ? '' : 'disabled' );
		$res_template .= '<input type="checkbox" class="car_order_fullday" data-class_for_autoselect="car_' . esc_attr( $post_id ) . '_order_day_' . esc_attr( $setday ) . '" ' . esc_attr( $disable ) . '>';
	}

	if ( $table_style == 'tableshort' ) {
		$res_template .= '<select id="car_' . $post_id . '_order_day_' . $setday . '" class="dscc_ttable_selldt" multiple>';
		for ( $ii = $dscc_time_from_rate; $ii < $dscc_time_to_rate; $ii ++ ) {
			$sethour   = strtotime( ' +' . $ii . '  hour', $setday );
			$freeHour  = ! in_array( $sethour, $dscc_timearrea );
			$disabled  = ( $freeDay && $freeHour && $featured ? '' : 'disabled' );
			$res_template .= '<option value="' . esc_attr( $sethour ) . '" ' . $disabled . '>' . esc_html( ( $ii < 10 ? 0 : '' ) . $ii . '.00' ) . '</option>';
		}
		$res_template .= '</select>';
	}

	if ( $table_style == 'dayly' ) {
		$res_template .= '<br /><input type="checkbox" class="car_order_allfullday" value="' . esc_attr( $setday ) . '" ' . ( in_array( $setday, $dscc_datearrea ) || ! $featured ? 'disabled' : '' ) . '>';
	}

	$res_template .= '</td>';
}
$res_template .= '</tr>';

if ( $table_style == 'tablefull' ) {
	for ( $i = $dscc_time_from_rate; $i < $dscc_time_to_rate; $i ++ ) {
		$res_template .= '<tr>';
		for ( $ii = 0; $ii < 9; $ii ++ ) {
			$setday    = strtotime( ' +' . $ii . ' day', $ts_start );
			$sethour   = strtotime( ' +' . $i . '  hour', $setday );
			$ynDay     = in_array( $setday, $dscc_datearrea );
			$ynHour    = in_array( $sethour, $dscc_timearrea );
			$featured  = ( $setday >= $today_ts ? true : false );
			$freeHour  = ( $ynDay || $ynHour ? false : true );
			$res_template .= '<td class="' . ( $freeHour && $featured ? 'dscc_bg_yes' : 'dscc_bg_none' ) . '">
								' . ( $i < 10 ? 0 : '' ) . $i . '.00 ' . '<input type="checkbox" class="car_order_' . esc_attr( $post_id ) . ' car_' . esc_attr( $post_id ) . '_order_day_' . esc_attr( $setday ) . '" value="' . esc_attr( $sethour ) . '" ' . ( $freeHour && $featured ? '' : 'disabled' ) . ' >
							  </td>';
		}
		$res_template .= '</tr>';
	}
}

$res_template .= '</table>
               	  <div class="car_shortcontent">' . get_the_excerpt( $post_id ) . '</div>';