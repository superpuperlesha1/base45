var SLIDEINDEX = 0;
var catGallFS  = '';

document.addEventListener('click', function(e){

	/* Slider function */
	function showSlides(index){
		var slides = catGallFS.querySelectorAll('.slide');
		var dots   = catGallFS.querySelectorAll('.dot-navigation');
		if (index >= slides.length){
			SLIDEINDEX = 0;
		} else if (index < 0) {
			SLIDEINDEX = slides.length - 1;
		} else{
			SLIDEINDEX = SLIDEINDEX;
		}
		for (var i = 0; i < slides.length; i++){
			slides[i].style.display = 'none';
			dots[i].classList.remove('active-dot');
		}
		slides[SLIDEINDEX].style.display = 'block';
		dots[SLIDEINDEX].classList.add('active-dot');
	};
	
	/* Start new slider */
	if( e.target.classList.contains('dscc_itemcar_sliderclicker') ) {
		SLIDEINDEX = 0;
		catGallFS  = document.getElementById( e.target.getAttribute('data-catslideboxid') );
		catGallFS.style.display = 'block';
		
		catGallFS.querySelector('#arrow-prev').addEventListener('click', function(){
			showSlides(--SLIDEINDEX);
		});

		catGallFS.querySelector('#arrow-next').addEventListener('click', function(){
			showSlides(++SLIDEINDEX);
		});

		catGallFS.querySelectorAll(".dot-navigation").forEach(function(element){
			element.addEventListener("click", function(){
				var dots = Array.prototype.slice.call(this.parentElement.children);
				var dotIndex = dots.indexOf(element);
				showSlides(SLIDEINDEX = dotIndex);
			});
		});

		showSlides( SLIDEINDEX );
	}
	
	/* Close slider */
	if( e.target.classList.contains('svphicats') ) {
		catGallFS.style.display = 'none';
	}
	
});